#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

# import threading;
from concurrent.futures import ThreadPoolExecutor, as_completed

from utils import soupify;

from database_management import *;
from database_management import *;

from database_insertion import *;

from parsing import *;

def scrape_gameday(url_base, yearspec= None, monthspec= None,
                   verbose= False, silent= False):

    if type(yearspec) == tuple and len(yearspec) == 2:
        yearmin= yearspec[0];
        yearmax= yearspec[1];
    else:
        yearmin= float('-inf');
        yearmax= float('inf');

    if type(monthspec) == int:
        monthmin= monthspec;
        monthmax= monthspec;
    else:
        monthmin= float('-inf');
        monthmax= float('inf');

    filename_log= "gameday.log";
    file_log= open(filename_log, "w");

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    if verbose:
      print("\nstarting with url_base: %s" % url_base);

    try:
        soup= soupify(url_base, encoding= "html.parser");
    except:
        file_log.write("Top level error!");
        file_log.write("\tFailed to open URL:");
        file_log.write("\t" + url_base);
        return;

    gameFound= False; # for debugging

    games= [];

    # should be cycling through the years at this level
    for link in soup.find_all('a'):

        yearlinktxt= link.get('href');

        # if the subdirectory does not represent a year, skip it
        if (yearlinktxt is None) or ("year_" not in yearlinktxt):
            continue;

        # strip out the year from the raw URL
        year= yearlinktxt.split('_')[1];
        year= year.split('/')[0];
        year= int(year);

        if year < yearmin: continue;
        if year > yearmax: continue;

        # the year URL put together
        url_year= url_base + yearlinktxt;

        if verbose:
          print(" year url: %s" % url_year);

        # req_year= requests.get(url_year);
        # data_year= req_year.text;
        # soup_year= BeautifulSoup(data_year, "html.parser");

        try:
            soup_year= soupify(url_year);
        except:
            # file_log.write("Year error!");
            # file_log.write("\tFailed to open year URL:");
            # file_log.write("\t" + url_year);
            continue;

        for link_year in soup_year.find_all('a'):
            monthlinktxt= link_year.get('href');

            # if the subdirectory isn't a month, we don't care about it
            if (monthlinktxt is None) or ("month_" not in monthlinktxt):
                continue;

            month= monthlinktxt.split('_')[1];
            month= month.split('/')[0];
            month= int(month);
            # print("%02d" % month);

            if month < monthmin: continue;
            if month > monthmax: continue;

            url_month= url_year + monthlinktxt;

            if verbose:
              print("  month url: %s" % url_month);

            # req_month= requests.get(url_month);
            # data_month= req_month.text;
            # soup_month= BeautifulSoup(data_month, "html.parser");

            try:
                soup_month= soupify(url_month);
            except:
                # file_log.write("Month error!");
                # file_log.write("\tFailed to open month URL:");
                # file_log.write("\t" + url_month);
                continue;

            def doDay(link_in):

                daylinktxt= link_in.get('href');

                if daylinktxt is None or "day_" not in daylinktxt:
                    return;

                day= daylinktxt.split('_')[1];
                day= day.split('/')[0];
                day= int(day);

                url_day= url_month + daylinktxt;

                # not sure why but you gotta cut off the last slash
                url_day= url_day[:-1];

                if verbose:
                  print("    day url: %s" % url_day);

                # req_day= requests.get(url_day);
                # data_day= req_day.text;
                # soup_day= BeautifulSoup(data_day, "html.parser");

                try:
                    soup_day= soupify(url_day);
                except:
                    # file_log.write("Day error!");
                    # file_log.write("\tFailed to open day URL:");
                    # file_log.write("\t" + url_day);
                    return;

                for link_day in soup_day.find_all('a'):
                    gamelinktxt= link_day.get('href');

                    if "master_scoreboard.xml" in gamelinktxt:
                        # for a given day, first parse the scoreboard and populate
                        # the game and inning tables fully before more granular data

                        url_masterscoreboard= url_month + gamelinktxt;

                        if verbose:
                          print("    master scoreboard found.");
                          print("        url: " + url_masterscoreboard);

                        try:
                            games_data= parseMasterScoreboardData( \
                                    url_masterscoreboard, verbose= False);
                        except:
                            # file_log.write("Master scoreboard error!");
                            # file_log.write("\tFailed to open scoreboard URL:");
                            # file_log.write("\t" + url_masterscoreboard);
                            continue;

                        for game_data in games_data:
                            cleanupGame(game_data);
                            insertGame(game_data, conn= conn);
                            insertInnings(game_data, conn= conn);

                            gameFound= True;

                            # THIS IS A PLACE WHERE MORE SHOULD BE DONE?

                    elif "gid_" in gamelinktxt and "grid_int.json" not in gamelinktxt:
                        url_game= url_month + gamelinktxt;
                        games.append(url_game);
                        print("        game found. gid: " \
                                + gamelinktxt.split('/')[1]);

                        # # req_inn= requests.get(url_game + "/inning/");
                        # # data_inn= req_inn.text;
                        # # soup_inn= BeautifulSoup(data_inn, "html.parser");
                        #
                        # soup_inn= soupify(url_game + "/inning/", encoding= "html.parser");
                        #
                        # for link_gameinn in soup_inn.find_all('a'):
                        #     inningtxt= link_gameinn.get('href');
                        #     print(inningtxt);

                    # # the below may be useful later, but for now is not
                    # elif "miniscoreboard.json" in gamelinktxt:
                    #     print("    mini scoreboard found.");
                    # elif "grid.json" in gamelinktxt:
                    #     print("    grid found.");

            # use multiple threads for avoiding timeouts
            with ThreadPoolExecutor(max_workers= 100) as pool:
                # a dictionary to map FROM future jobs TO the results
                results= {};
                # submit jobs to be done
                future_to_link= {pool.submit(doDay, link): link for link in
                        soup_month.find_all('a')};
                # loop over jobs to be done
                for future in as_completed(future_to_link):
                    # get the result
                    link= future_to_link[future];

                    # get the data out
                    data= future.result();

                    # actually unpack it
                    results[link]= data;
                pool.shutdown(wait= False);

        # break; # DEBUG

    # ##### DEBUG!!!!! #####
    # games= ['http://gd2.mlb.com/components/game/mlb/year_2019/month_08/day_10/gid_2019_08_10_kcamlb_detmlb_1/',];

    print('games:');
    for game in sorted(games):
        print('\t' + game);

    def doGame(game):
        # request and unpack a single MLB game

        if verbose:
            print("    gid to be treated: %s" % game[:-1].split('/')[-1]);

        # # old way
        # req_inn= requests.get(game + "inning");
        # data_inn= req_inn.text;
        # soup_inn= BeautifulSoup(data_inn, "html.parser");

        # convert to beautifulsoup
        soup_inn= soupify(game + "inning", encoding= "html.parser");

        # list of returns
        returnable= [];

        # loop over inning links in a game
        for link_gameinn in soup_inn.find_all('a'):

            inningtxt= link_gameinn.get('href');

            # inning_all files have everything we need
            if "inning_all" not in inningtxt:
                continue;

            # print("        " + inningtxt);

            url_inningall= game + inningtxt;
            # print("        " + url_inningall);

            # convert raw to useful data format
            # atbat_data= parseGameData(game, logfile= file_log);
            master_data= parseGameData(game);
            atbat_data= master_data['atbat'];
            runner_data= master_data['runner'];

            # add to the collection to return
            returnable.append((atbat_data, runner_data));

        # # old way, now we save for single write to postgres
        # insertAtbats(atbat_data, conn= conn, verbose= True);

        # doGame.count += 1;
        # if not silent:
        #     print("%f\%: game %d of %d" % (doGame.count/doGame.total,
        #             doGame.count, doGame.total));
        return returnable;
    # doGame.count= 0;
    # doGame.total= len(games);

    # results= {};
    # for game in games:
    #     results[game]= doGame(game);


    # use multiple threads for avoiding timeouts
    with ThreadPoolExecutor(max_workers= 100) as pool:

        # a dictionary to map FROM future jobs TO the results
        results= {};

        # submit jobs to be done
        future_to_game= {pool.submit(doGame, game): game for game in games};
        print("starting the parallel part...");
        # loop over jobs to be done
        for future in as_completed(future_to_game):
            # get the result
            game= future_to_game[future];
            print("%s processed." % game);

            # # get the data out
            try:
                data= future.result();
                # actually unpack it
                results[game]= data;
            except Exception as e:
                print(e);
                file_log.write("Game read error:");
                file_log.write("\tgid- " + game + "\n\n");
                continue;

        # pool.shutdown();

        # now rip through the dict and proberly unpack
        for game, result in results.items():
            # try:
            print(game);
            for master_data in result:
                insertAtbats(master_data[0], conn= conn);
                # insertAtbats(master_data[0], conn= conn, verbose= True);
                for data in master_data[1]:
                    insertRunners(data, conn= conn);
                    # insertRunners(data, conn= conn, verbose= True);
            # except:
            #     file_log.write("Game read error:");
            #     file_log.write("\tgid- " + game + "\n\n");
            #     # continue;
            #     raise;

def scrape_gameday_old(url_base, verbose= False, silent= False):

    filename_log= "gameday.log";
    file_log= open(filename_log, "w");

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    if verbose:
      print("\nstarting with url_base: %s" % url_base);

    # # pull request and parse as text
    # req= requests.get(url_base);
    # data= req.text;
    #
    # # constructor
    # soup= BeautifulSoup(data, "html.parser");

    try:
        soup= soupify(url_base, encoding= "html.parser");
    except:
        file_log.write("Top level error!");
        file_log.write("\tFailed to open URL:");
        file_log.write("\t" + url_base);
        return;

    gameFound= False; # for debugging

    # should be cycling through the years at this level
    for link in soup.find_all('a'):

        yearlinktxt= link.get('href');

        # if the subdirectory does not represent a year, skip it
        if (yearlinktxt is None) or ("year_" not in yearlinktxt):
            continue;

        # strip out the year from the raw URL
        year= yearlinktxt.split('_')[1];
        year= year.split('/')[0];
        year= int(year);

        # the year URL put together
        url_year= url_base + yearlinktxt;

        if verbose:
          print(" year url: %s" % url_year);

        # req_year= requests.get(url_year);
        # data_year= req_year.text;
        # soup_year= BeautifulSoup(data_year, "html.parser");

        try:
            soup_year= soupify(url_year);
        except:
            file_log.write("Year error!");
            file_log.write("\tFailed to open year URL:");
            file_log.write("\t" + url_year);
            continue;

        for link_year in soup_year.find_all('a'):
            monthlinktxt= link_year.get('href');

            # if the subdirectory isn't a month, we don't care about it
            if (monthlinktxt is None) or ("month_" not in monthlinktxt):
                continue;

            month= monthlinktxt.split('_')[1];
            month= month.split('/')[0];
            month= int(month);
            # print("%02d" % month);

            url_month= url_year + monthlinktxt;

            if verbose:
              print("  month url: %s" % url_month);

            # req_month= requests.get(url_month);
            # data_month= req_month.text;
            # soup_month= BeautifulSoup(data_month, "html.parser");

            try:
                soup_month= soupify(url_month);
            except:
                file_log.write("Month error!");
                file_log.write("\tFailed to open month URL:");
                file_log.write("\t" + url_month);
                continue;

            for link_month in soup_month.find_all('a'):
                daylinktxt= link_month.get('href');

                if daylinktxt is None or "day_" not in daylinktxt:
                    continue;

                day= daylinktxt.split('_')[1];
                day= day.split('/')[0];
                day= int(day);

                url_day= url_month + daylinktxt;

                # not sure why but you gotta cut off the last slash
                url_day= url_day[:-1];

                if verbose:
                  print("    day url: %s" % url_day);

                # req_day= requests.get(url_day);
                # data_day= req_day.text;
                # soup_day= BeautifulSoup(data_day, "html.parser");

                try:
                    soup_day= soupify(url_day);
                except:
                    file_log.write("Day error!");
                    file_log.write("\tFailed to open day URL:");
                    file_log.write("\t" + url_day);
                    continue;

                games= [];

                for link_day in soup_day.find_all('a'):
                    gamelinktxt= link_day.get('href');

                    if "master_scoreboard.xml" in gamelinktxt:
                        # for a given day, first parse the scoreboard and populate
                        # the game and inning tables fully before more granular data

                        url_masterscoreboard= url_month + gamelinktxt;

                        if verbose:
                          print("    master scoreboard found.");
                          print("        url: " + url_masterscoreboard);

                        try:
                            games_data= parseMasterScoreboardData( \
                                    url_masterscoreboard, verbose= False);
                        except:
                            file_log.write("Master scoreboard error!");
                            file_log.write("\tFailed to open scoreboard URL:");
                            file_log.write("\t" + url_masterscoreboard);
                            continue;

                        for game_data in games_data:
                            cleanupGame(game_data);
                            insertGame(game_data, conn= conn);
                            insertInnings(game_data, conn= conn);

                            gameFound= True;

                            # THIS IS A PLACE WHERE MORE SHOULD BE DONE?

                    elif "gid_" in gamelinktxt and "grid_int.json" not in gamelinktxt:
                        url_game= url_month + gamelinktxt;
                        games.append(url_game);
                        print("        game found. gid: " \
                                + gamelinktxt.split('/')[1]);

                        # # req_inn= requests.get(url_game + "/inning/");
                        # # data_inn= req_inn.text;
                        # # soup_inn= BeautifulSoup(data_inn, "html.parser");
                        #
                        # soup_inn= soupify(url_game + "/inning/", encoding= "html.parser");
                        #
                        # for link_gameinn in soup_inn.find_all('a'):
                        #     inningtxt= link_gameinn.get('href');
                        #     print(inningtxt);

                    # # the below may be useful later, but for now is not
                    # elif "miniscoreboard.json" in gamelinktxt:
                    #     print("    mini scoreboard found.");
                    # elif "grid.json" in gamelinktxt:
                    #     print("    grid found.");

                def doGame(game):
                    # request and unpack a single MLB game

                    if verbose:
                        print("    gid to be treated: %s" % game[:-1].split('/')[-1]);

                    # # old way
                    # req_inn= requests.get(game + "inning");
                    # data_inn= req_inn.text;
                    # soup_inn= BeautifulSoup(data_inn, "html.parser");

                    # convert to beautifulsoup
                    soup_inn= soupify(game + "inning", encoding= "html.parser");

                    # list of returns
                    returnable= [];

                    # loop over inning links in a game
                    for link_gameinn in soup_inn.find_all('a'):

                        inningtxt= link_gameinn.get('href');

                        # inning_all files have everything we need
                        if "inning_all" not in inningtxt:
                            continue;

                        # print("        " + inningtxt);

                        url_inningall= game + inningtxt;
                        # print("        " + url_inningall);

                        # convert raw to useful data format
                        atbat_data= parseGameData(game);
                        # atbat_data= parseGameData(game, logfile= file_log);
                        atbat_data= atbat_data['atbat'];

                        # add to the collection to return
                        returnable.append(atbat_data);

                    # # old way, now we save for single write to postgres
                    # insertAtbats(atbat_data, conn= conn, verbose= True);

                    return returnable;

                # use multiple threads for avoiding timeouts
                with ThreadPoolExecutor(max_workers= 20) as pool:
                    # a dictionary to map FROM future jobs TO the results
                    results= {};
                    # submit jobs to be done
                    future_to_game= {pool.submit(doGame, game): game for game in games};
                    # loop over jobs to be done
                    for future in as_completed(future_to_game):
                        # get the result
                        game= future_to_game[future];

                        # get the data out
                        data= future.result();

                        # actually unpack it
                        results[game]= data;

                    # now rip through the dict and proberly unpack
                    for game, result in results.items():
                        try:
                            for atbat_data in result:
                                insertAtbats(atbat_data, conn= conn, verbose= True);
                        except:
                            file_log.write("Game read error:");
                            file_log.write("\tgid- " + gamelinktxt + "\n\n");
                            # continue;
                            raise;

            # # JUST DO ONE MONTH WHILE BUILDING CAPABILITIES
            # if gameFound:
            #     break;

        # JUST DO ONE YEAR WHILE BUILDING CAPABILITIES
        if gameFound:
            break;

    if verbose:
      print("\n");

    conn.close();
    if not silent:
        print("\npersistent database closed successfully!\n");

    file_log.close();
