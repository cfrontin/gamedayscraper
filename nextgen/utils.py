#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

def base_state_empty():
    return {'1B': [], '2B': [], '3B': [],};

def game_keys():
    game_keys= [
        'gamePk',
        'awayId',
        'homeId',
        'awayTeamName',
        'homeTeamName',
        'awayGamesPlayed',
        'homeGamesPlayed',
        'awayWins',
        'homeWins',
        'awayLosses',
        'homeLosses',
        'awayGB',
        'homeGB',
        'awayWCGB',
        'homeWCGB',
        'awayRuns',
        'homeRuns',
        'awayHits',
        'homeHits',
        'awayErrors',
        'homeErrors',
        'doubleHeader',
        'dateTime',
        'gameNumber',
        'gameType',
        'sportId',
        'venueId',
        'originalDate',
        'statusCode',
        'season',
        'wxCondition',
        'wxTemp',
        'wxWind',
    ];

    game_pks= ['gamePk',];

    return (game_keys, game_pks);

def inning_keys():
    inning_keys= [
        'gamePk',
        'inningNum',
        'awayRuns',
        'homeRuns',
        'awayHits',
        'homeHits',
        'awayErrors',
        'homeErrors',
        'awayLOB',
        'homeLOB',
        'awayTotalRuns',
        'homeTotalRuns',
        'awayTotalHits',
        'homeTotalHits',
        'awayTotalErrors',
        'homeTotalErrors',
        'awayTotalLOB',
        'homeTotalLOB',
    ];

    inning_pks= ['gamePk', 'inningNum',];

    return (inning_keys, inning_pks);

def atbat_keys():
    atbat_keys= [
        'gamePk',
        'playNum',
        'inningNum',
        'inningHalf',
        'batterId',
        'batterSide',
        'pitcherId',
        'pitcherHand',
        'awayScore',
        'homeScore',
        'awayScorePre',
        'homeScorePre',
        'b',
        's',
        'o',
        'event',
        'eventType',
        'startTime',
        'endTime',
        'description',
        'oPre',
        'runsScored',
        'rbi',
        'earnedRuns',
        'on1Bpre',
        'on2Bpre',
        'on3Bpre',
        'on1Bpost',
        'on2Bpost',
        'on3Bpost',
    ];

    atbat_pks= ['gamePk', 'playNum',];

    return (atbat_keys, atbat_pks);

def db_keys(tablename):
    if tablename == 'atbat':
        return atbat_keys();
    elif tablename == 'inning':
        return inning_keys();
    elif tablename == 'game':
        return game_keys();
    elif tablename == 'pitch':
        raise ImplementationError("implement! -cfrontin");
        return pitch_keys();
    elif tablename == 'hit':
        raise ImplementationError("implement! -cfrontin");
        return hit_keys();
    else:
        raise LookupError("table %s keys not found!" % tablename);


#
