#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import json;
import urllib.request;
import pprint;
import time;

from database_management import openGamedayDatabase;
from database_management import closeGamedayDatabase;
from database_management import createCustomTypes;
from database_management import createGameTable;
from database_management import createInningTable;
from database_management import createAtbatTable;

from database_insertion import upsert_rows;

from parsing import parseGame;

from schedule import generate_gamelist;

verbose= True;
silent= False;

# INITIALIZE DATABASE

# open that baby up
(conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

# custom types and prep
createCustomTypes(conn= conn, verbose= verbose, silent= silent);

# create the tables
createGameTable(conn= conn, verbose= verbose, silent= silent);
createInningTable(conn= conn, verbose= verbose, silent= silent);
createAtbatTable(conn= conn, verbose= verbose, silent= silent);

# GENERATE GAME LIST

sportId= 1;

game_list= generate_gamelist(season= 2019, startDate= "2019-05-01",
        endDate= "2019-05-31");
# game_list= [566413, 565541, 565337,];
# game_list= [565337,];
# in the future, we want actually get these from... somewhere?

# loop over games
game_count= 0;
for game in game_list:
    # get the data for a given game
    (output_game, output_innings, output_atbats)= parseGame(game, sportId,
            conn= conn, verbose= verbose, silent= silent);
    # upsert into database
    upsert_rows(conn, "game", output_game, verbose= verbose, silent= silent);
    upsert_rows(conn, "inning", output_innings, verbose= verbose, silent= silent);
    upsert_rows(conn, "atbat", output_atbats, verbose= verbose, silent= silent);
    game_count += 1;
    print("\tgame %d of %d processed." % (game_count, len(game_list)));
    time.sleep(1.0);

# close out
closeGamedayDatabase(conn, verbose= verbose, silent= silent);

#
