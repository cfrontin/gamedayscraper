#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import json;
import urllib.request;
import pprint;

gameno= 566413;
# gameno= 565541; # has steals
# gameno= 565337; # has pinch runner
gameurl= "https://statsapi.mlb.com/api/v1.1/game/%06d/feed/live" % gameno;
with urllib.request.urlopen(gameurl) as request:
    gamedata= json.loads(request.read().decode());

pp= pprint.PrettyPrinter(indent= 2);

# pp.pprint(gamedata['liveData']['plays']['allPlays']);

### PARSE A GAME

# top level

# primary key of the game
gamePk= gamedata['gamePk'];

# gameData

gameData= gamedata['gameData'];

gameType= gamedata['gameData']['game']['type'];
doubleHeader= gamedata['gameData']['game']['doubleHeader'];
gameId= gamedata['gameData']['game']['id']; # like gamedaylink before
tiebreaker= gamedata['gameData']['game']['tiebreaker'];
gameNumber= gamedata['gameData']['game']['gameNumber'];
season= gamedata['gameData']['game']['season'];
seasonDisplay= gamedata['gameData']['game']['seasonDisplay'];

# datetime
dateTime= gamedata['gameData']['datetime']['dateTime']; # good for python datetime
originalDate= gamedata['gameData']['datetime']['originalDate'];
dayNight= gamedata['gameData']['datetime']['dayNight'];
time= gamedata['gameData']['datetime']['time'];
ampm= gamedata['gameData']['datetime']['ampm'];

# status
abstractGameState= gamedata['gameData']['status']['abstractGameState'];
codedGameState= gamedata['gameData']['status']['codedGameState'];
detailedState= gamedata['gameData']['status']['detailedState'];
statusCode= gamedata['gameData']['status']['statusCode'];
abstractGameCode= gamedata['gameData']['status']['abstractGameCode'];

# teams

awayId= gamedata['gameData']['teams']['away']['id'];
awayName= gamedata['gameData']['teams']['away']['name'];
awayLeagueId= gamedata['gameData']['teams']['away']['league']['id']; # need to populate league table (see GUMBO)
awayDivisionId= gamedata['gameData']['teams']['away']['division']['id']; # need to populate division table (see GUMBO)
awaySportId= gamedata['gameData']['teams']['away']['sport']['id']; # need to populate sport table (see GUMBO)
awaySeason= gamedata['gameData']['teams']['away']['season'];
awayTeamCode= gamedata['gameData']['teams']['away']['teamCode'];
awayAbbreviation= gamedata['gameData']['teams']['away']['abbreviation'];
awayTeamName= gamedata['gameData']['teams']['away']['teamName'];
awayLocationName= gamedata['gameData']['teams']['away']['locationName'];

homeId= gamedata['gameData']['teams']['home']['id'];
homeName= gamedata['gameData']['teams']['home']['name'];
homeLeagueId= gamedata['gameData']['teams']['home']['league']['id']; # need to populate league table (see GUMBO)
homeDivisionId= gamedata['gameData']['teams']['home']['division']['id']; # need to populate division table (see GUMBO)
homeSportId= gamedata['gameData']['teams']['home']['sport']['id']; # need to populate sport table (see GUMBO)
homeLink= gamedata['gameData']['teams']['home']['link'];
homeSeason= gamedata['gameData']['teams']['home']['season'];
homeTeamCode= gamedata['gameData']['teams']['home']['teamCode'];
homeAbbreviation= gamedata['gameData']['teams']['home']['abbreviation'];
homeTeamName= gamedata['gameData']['teams']['home']['teamName'];
homeLocationName= gamedata['gameData']['teams']['home']['locationName'];

# teams: record edition

awayGamesPlayed= gamedata['gameData']['teams']['away']['record']['gamesPlayed'];
awayWins= gamedata['gameData']['teams']['away']['record']['wins'];
awayLosses= gamedata['gameData']['teams']['away']['record']['losses'];
homeGamesPlayed= gamedata['gameData']['teams']['home']['record']['gamesPlayed'];
homeWins= gamedata['gameData']['teams']['home']['record']['wins'];
homeLosses= gamedata['gameData']['teams']['home']['record']['losses'];

# MAY WANT TO ADD MORE DETAILS ABOUT RECORD HERE IN THE FUTURE...

# venue

venueId= gamedata['gameData']['venue']['id'];
venueName= gamedata['gameData']['venue']['name'];
venueLink= gamedata['gameData']['venue']['link'];
venueLocationCity= gamedata['gameData']['venue']['location']['city'];
venueLocationState= gamedata['gameData']['venue']['location']['state'];
venueLocationStateAbbrev= gamedata['gameData']['venue']['location']['stateAbbrev'];
venueLocationLatitude= gamedata['gameData']['venue']['location']['defaultCoordinates']['latitude'];
venueLocationLongitude= gamedata['gameData']['venue']['location']['defaultCoordinates']['longitude'];

# weather

wxCondition= gamedata['gameData']['weather']['condition'];
wxTemp= gamedata['gameData']['weather']['temp'];
wxWind= gamedata['gameData']['weather']['wind'];

# flags

noHitter= gamedata['gameData']['flags']['noHitter'];
perfectGame= gamedata['gameData']['flags']['perfectGame'];

# boxscore

# there might be some interesting stuff here... but for now I'm not too worried
# about it. the most intersting is the team stats for the game, which, I presume
# are the teams' stats up to this game. this might be interesting for some kinds
# of matchup modeling.

# linescore

line_home_runs= gamedata['liveData']['linescore']['teams']['home']['runs'];
line_home_hits= gamedata['liveData']['linescore']['teams']['home']['hits'];
line_home_errors= gamedata['liveData']['linescore']['teams']['home']['errors'];
line_home_lob= gamedata['liveData']['linescore']['teams']['home']['leftOnBase'];

line_away_runs= gamedata['liveData']['linescore']['teams']['away']['runs'];
line_away_hits= gamedata['liveData']['linescore']['teams']['away']['hits'];
line_away_errors= gamedata['liveData']['linescore']['teams']['away']['errors'];
line_away_lob= gamedata['liveData']['linescore']['teams']['away']['leftOnBase'];

line_innings= gamedata['liveData']['linescore']['innings'];

# accumulated runs
inn_homeTotalRuns= 0;
inn_homeTotalHits= 0;
inn_homeTotalErrors= 0;
inn_homeTotalLOB= 0;

inn_awayTotalRuns= 0;
inn_awayTotalHits= 0;
inn_awayTotalErrors= 0;
inn_awayTotalLOB= 0;

for inning in line_innings:

    inningNum= inning['num'];
    innOrdinalNum= inning['ordinalNum'];

    inn_homeRuns= inning['home']['runs'] if 'runs' in inning['home'].keys() else 0;
    inn_homeHits= inning['home']['hits'];
    inn_homeErrors= inning['home']['errors'];
    inn_homeLOB= inning['home']['leftOnBase'];

    inn_awayRuns= inning['away']['runs'];
    inn_awayHits= inning['away']['hits'];
    inn_awayErrors= inning['away']['errors'];
    inn_awayLOB= inning['away']['leftOnBase'];

    # accumulate
    inn_homeTotalRuns += inn_homeRuns;
    inn_homeTotalHits += inn_homeHits;
    inn_homeTotalErrors += inn_homeErrors;
    inn_homeTotalLOB += inn_homeLOB;

    inn_awayTotalRuns += inn_awayRuns;
    inn_awayTotalHits += inn_awayHits;
    inn_awayTotalErrors += inn_awayErrors;
    inn_awayTotalLOB += inn_awayLOB;

    # ready to code innings into the database

# decisions

if 'winner' in gamedata['liveData']['decisions']:
    decide_winner_id= gamedata['liveData']['decisions']['winner']['id'];
    decide_winner_fullName= gamedata['liveData']['decisions']['winner']['fullName'];
else:
    decide_winner_id= None;
    decide_winner_fullName= None;

if 'loser' in gamedata['liveData']['decisions']:
    decide_loser_id= gamedata['liveData']['decisions']['loser']['id'];
    decide_loser_fullName= gamedata['liveData']['decisions']['loser']['fullName'];
else:
    decide_loser_id= None;
    decide_loser_fullName= None;

if 'save' in gamedata['liveData']['decisions']:
    decide_save_id= gamedata['liveData']['decisions']['save']['id'];
    decide_save_fullName= gamedata['liveData']['decisions']['save']['fullName'];
else:
    decide_save_id= None;
    decide_save_fullName= None;

# atbats

play_types= {};

inning_state= {'num': 1, 'istop': True};
out_state= 0;
base_state= {'1B': [], '2B': [], '3B': []};
score_state= {'away': 0, 'home': 0};

movementreason_list= {};

for playNum in range(len(gamedata['liveData']['plays']['allPlays'])):

    play= gamedata['liveData']['plays']['allPlays'][playNum];

    # each entry is one raw plate appearance or onfield event

    play_type= play['result']['type'];
    play_event= play['result']['event'];
    play_eventtype= play['result']['eventType'];
    play_description= play['result']['description'];
    play_rbi= play['result']['rbi'];

    play_index= play['about']['atBatIndex'];
    play_halfinn= play['about']['halfInning'];
    play_inn= play['about']['inning'];
    play_starttime= play['about']['startTime'];
    play_endtime= play['about']['endTime'];

    # quick interruption to update the inning state
    newinning= False;
    # print(play_halfinn);
    # print(inning_state);
    if ((play_inn != inning_state['num']) or
            (play_halfinn != ("top" if inning_state['istop'] else "bottom"))):
        newinning= True;

        if inning_state['istop']:

            # consistency check to validate the left-on-base numbers
            testlob= 0;
            for base in base_state:
                testlob += len(base_state[base]);
            assert testlob == \
                    line_innings[inning_state['num'] - 1]['away']['leftOnBase'];

            inning_state['istop']= False;
        else:
            # consistency check to validate the left-on-base numbers
            testlob= 0;
            for base in base_state:
                testlob += len(base_state[base]);
            assert testlob == \
                    line_innings[inning_state['num'] - 1]['home']['leftOnBase'];

            inning_state['istop']= True;
            inning_state['num'] += 1;
        out_state= 0;
        base_state_pre= {'1B': [], '2B': [], '3B': [],};
    else:
        base_state_pre= base_state.copy();
    # and now back to your regularly scheduled programming

    play_startTime= play['about']['startTime'];
    play_endTime= play['about']['endTime'];
    play_iscomplete= play['about']['isComplete'];
    play_isscoring= play['about']['isScoringPlay'];
    play_hasreview= play['about']['hasReview'];
    play_hasout= play['about']['hasOut'];

    play_awayScore= play['result']['awayScore'];
    play_homeScore= play['result']['homeScore'];

    play_count= {'balls': play['count']['balls'],
            'strikes': play['count']['strikes'],
            'outs': play['count']['outs']};

    play_batterId= play['matchup']['batter']['id'];
    play_batterSide= play['matchup']['batSide']['code'];
    play_pitcherId= play['matchup']['pitcher']['id'];
    play_pitcherHand= play['matchup']['pitchHand']['code'];

    play_split_batter= play['matchup']['splits']['batter'];
    play_split_pitcher= play['matchup']['splits']['pitcher'];
    play_split_menonbase= play['matchup']['splits']['menOnBase'];

    play_pitch_indices= play['pitchIndex'];
    play_action_indices= play['actionIndex'];

    # runners as always... try to filter by movementReason and event to classify
    # pre-play, play, and post-play movement

    # # don't think i actually need to do this...
    play_nplayevent= len(play['playEvents']);

    play_event_runners= []; # list for each event index of the runners for that event

    runner_map= [];
    for i in range(play_nplayevent):
        event_runners= [];
        for j in range(len(play['runners'])):
            runner= play['runners'][j];
            if runner['details']['playIndex'] == i:
                event_runners.append(j);
                runner_map.append(j);
        play_event_runners.append(event_runners);

    assert sorted(runner_map) == list(range(len(play['runners'])));

    def do_runner(runner, out_state):

        runs_scored= 0;

        runner_event= runner['details']['event'];
        movementreason= runner['details']['movementReason'];

        if runner['movement']['isOut']:
            out_state += 1;

        runner_id= runner['details']['runner']['id'];
        runner_start= runner['movement']['start'];
        runner_end= runner['movement']['end'];
        runner_isout= runner['movement']['isOut'];
        runner_outno= runner['movement']['outNumber'];

        base_state_add= {'1B': [], '2B': [], '3B': [],};
        base_state_remove= {'1B': [], '2B': [], '3B': [],};
        if runner_start is not None:
            base_state_remove[runner_start].append(runner_id);
        if runner_end in ['1B', '2B', '3B']:
            base_state_add[runner_end].append(runner_id);
        if runner_end == "score":
            runs_scored += 1;

        return (out_state, base_state_add, base_state_remove, runs_scored);

    # loop over play-events
    for eventno in range(play_nplayevent):
        # loop to move runners

        # credit/debit for this play event on the base-out state
        event_base_state_add= {'1B': [], '2B': [], '3B': [],};
        event_base_state_remove= {'1B': [], '2B': [], '3B': [],};

        playevent= play['playEvents'][eventno];
        if (playevent['type'] == "action") and \
                (playevent['details']['eventType'] == "offensive_substitution") \
                and (playevent['position']['abbreviation'] == "PR"):
            substitute= playevent['player']['id'];
            print(playevent);
            base_sub= ['1B', '2B', '3B'][playevent['base'] - 1];
            assert (len(base_state[base_sub]) == 1);
            print("%s should be substituted for %s at %s" % (substitute,
                    base_state[base_sub][0], substitute));
            event_base_state_remove[base_sub].append(base_state[base_sub][0]);
            event_base_state_add[base_sub].append(substitute);

        # loop over runners in this play event
        for idx_runner in range(len(play_event_runners[eventno])):
            runner= play['runners'][play_event_runners[eventno][idx_runner]];

            # do the math
            (out_state, base_state_add, base_state_remove, runs_scored)= \
                    do_runner(runner, out_state);

            # make sure it's working
            assert base_state_pre.keys() == base_state_add.keys() \
                    == base_state_remove.keys();

            # do credit/debit accounting
            for base in base_state_add.keys():
                for baserunner in base_state_add[base]:
                    event_base_state_add[base].append(baserunner);
            for base in base_state_remove.keys():
                for baserunner in base_state_remove[base]:
                    event_base_state_remove[base].append(baserunner);
            score_state['away' if inning_state['istop'] else 'home'] += \
                    runs_scored;

        # ok now let's file the play event
        base_state= base_state_pre;

        # work through the accounting
        for base in base_state.keys():
            for baserunner in event_base_state_add[base]:
                base_state[base].append(baserunner);

        for base in base_state.keys():
            for baserunner in event_base_state_remove[base]:
                base_state[base].remove(baserunner);

        # copy into the new pre-state
        base_state_pre= base_state.copy();

    # sanity-check- one man per base
    for base in base_state.keys():
        assert len(base_state[base]) in [0, 1];

    # sanity-check- scores should be accounted for
    if inning_state['istop']:
        assert score_state['away'] == play_awayScore;
    else:
        assert score_state['home'] == play_homeScore;

    # sanity-check- out state should be correct
    assert out_state == play_count['outs'];

    # sanity-check- make sure play no matches at-bat index (should always, i think)
    assert playNum == play['atBatIndex'];

    # output for debugging
    print("play no.: %d" % playNum);
    print("\tinning: %s%d" % ("top" if inning_state['istop'] else "bottom",
            inning_state['num']));
    print("\touts: %d" % out_state);
    print("\t1B: %d, 2B: %d, 3B: %d" % (len(base_state['1B']),
            len(base_state['2B']), len(base_state['3B']),));

    # some investigations of values
    if play_type not in play_types.keys():
        play_types[play_type]= 1;
    else:
        play_types[play_type] += 1;

# print("movement reasons collected:");
# for (mvtrz, no) in movementreason_list.items():
#     print("'%s': %d" % (mvtrz, no));
#
# print("play types collected:");
# for play_type in play_types:
#     print("%s" % play_type);

print(gamePk);

print(awayId);











#
