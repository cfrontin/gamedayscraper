#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import json;
import urllib.request;

from utils import base_state_empty;

def do_runner(runner, out_state):

    out_log= [];

    runner_scored= 0;

    runner_eventType= runner['details']['eventType'];
    movementreason= runner['details']['movementReason'];

    runner_id= runner['details']['runner']['id'];
    runner_start= runner['movement']['start'];
    runner_end= runner['movement']['end'];
    runner_isout= runner['movement']['isOut'];
    runner_outno= runner['movement']['outNumber'];
    runner_rbi= 1 if runner['details']['rbi'] else 0;
    runner_earnedRun= 1 if runner['details']['rbi'] else 0;
    runner_teamUnearnedRun= 1 if runner['details']['rbi'] else 0;

    if runner['movement']['isOut']:
        out_log.append(runner_id);

    base_state_add= base_state_empty();
    base_state_remove= base_state_empty();
    if runner_start is not None:
        base_state_remove[runner_start].append(runner_id);
    if runner_end in ['1B', '2B', '3B']:
        base_state_add[runner_end].append(runner_id);
    if runner_end == "score":
        runner_scored += 1;

    return (out_log, base_state_add, base_state_remove, runner_scored,
            runner_rbi, runner_earnedRun, runner_teamUnearnedRun,
            runner_eventType);

def parseGame(gamePk, sportId, conn= None, verbose= False, silent= False):
    """
    shred a GUMBO game file, turning it into a game

    parameters
    ----------
    gamePk : string
        the primary key for the game of interest
    conn : psycopg2 connection
        psycopg2 connection, needing a cursor. defaults to None
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?

    returns
    -------
    ??? : ???
        dictionary of game data for a single game or something
    """

    # generate a url and get the data
    url_game= "https://statsapi.mlb.com/api/v1.1/game/%06d/feed/live" % gamePk;

    with urllib.request.urlopen(url_game) as request:
        gamedata= json.loads(request.read().decode());

    # create structure for the long-run data
    output_game= {}; # rows a la {'db_colname': data, ...}
    output_innings= []; # list of inning rows as above
    output_atbats= []; # list of atbat rows as above
    output_runners= []; # list of runner rows as above... TBD
    output_pitches= []; # list of pitch rows as above... TBD
    output_hits= []; # list of hit rows as above... TBD

    ### PARSE A GAME

    # top level

    # primary key of the game
    gamePk= gamedata['gamePk'];

    # gameData

    gameData= gamedata['gameData'];

    gameType= gamedata['gameData']['game']['type'];
    doubleHeader= gamedata['gameData']['game']['doubleHeader'];
    gameId= gamedata['gameData']['game']['id']; # like gamedaylink before
    tiebreaker= gamedata['gameData']['game']['tiebreaker'];
    gameNumber= gamedata['gameData']['game']['gameNumber'];
    season= gamedata['gameData']['game']['season'];
    seasonDisplay= gamedata['gameData']['game']['seasonDisplay'];

    # datetime
    dateTime= gamedata['gameData']['datetime']['dateTime']; # good for python datetime
    originalDate= gamedata['gameData']['datetime']['originalDate'];
    dayNight= gamedata['gameData']['datetime']['dayNight'];
    time= gamedata['gameData']['datetime']['time'];
    ampm= gamedata['gameData']['datetime']['ampm'];

    # status
    abstractGameState= gamedata['gameData']['status']['abstractGameState'];
    codedGameState= gamedata['gameData']['status']['codedGameState'];
    detailedState= gamedata['gameData']['status']['detailedState'];
    statusCode= gamedata['gameData']['status']['statusCode'];
    abstractGameCode= gamedata['gameData']['status']['abstractGameCode'];

    # teams
    awayId= gamedata['gameData']['teams']['away']['id'];
    awayName= gamedata['gameData']['teams']['away']['name'];
    awayLeagueId= gamedata['gameData']['teams']['away']['league']['id']; # need to populate league table (see GUMBO)
    awayDivisionId= gamedata['gameData']['teams']['away']['division']['id']; # need to populate division table (see GUMBO)
    awaySportId= gamedata['gameData']['teams']['away']['sport']['id']; # need to populate sport table (see GUMBO)
    awaySeason= gamedata['gameData']['teams']['away']['season'];
    awayTeamCode= gamedata['gameData']['teams']['away']['teamCode'];
    awayAbbreviation= gamedata['gameData']['teams']['away']['abbreviation'];
    awayTeamName= gamedata['gameData']['teams']['away']['teamName'];
    awayLocationName= gamedata['gameData']['teams']['away']['locationName'];

    homeId= gamedata['gameData']['teams']['home']['id'];
    homeName= gamedata['gameData']['teams']['home']['name'];
    homeLeagueId= gamedata['gameData']['teams']['home']['league']['id']; # need to populate league table (see GUMBO)
    homeDivisionId= gamedata['gameData']['teams']['home']['division']['id']; # need to populate division table (see GUMBO)
    homeSportId= gamedata['gameData']['teams']['home']['sport']['id']; # need to populate sport table (see GUMBO)
    homeLink= gamedata['gameData']['teams']['home']['link'];
    homeSeason= gamedata['gameData']['teams']['home']['season'];
    homeTeamCode= gamedata['gameData']['teams']['home']['teamCode'];
    homeAbbreviation= gamedata['gameData']['teams']['home']['abbreviation'];
    homeTeamName= gamedata['gameData']['teams']['home']['teamName'];
    homeLocationName= gamedata['gameData']['teams']['home']['locationName'];

    # teams: record edition

    awayGamesPlayed= gamedata['gameData']['teams']['away']['record']['gamesPlayed'];
    awayWins= gamedata['gameData']['teams']['away']['record']['wins'];
    awayLosses= gamedata['gameData']['teams']['away']['record']['losses'];
    awayGB= gamedata['gameData']['teams']['away']['record']['divisionGamesBack'];
    awayWCGB= gamedata['gameData']['teams']['away']['record']['wildCardGamesBack'];

    homeGamesPlayed= gamedata['gameData']['teams']['home']['record']['gamesPlayed'];
    homeWins= gamedata['gameData']['teams']['home']['record']['wins'];
    homeLosses= gamedata['gameData']['teams']['home']['record']['losses'];
    homeGB= gamedata['gameData']['teams']['home']['record']['divisionGamesBack'];
    homeWCGB= gamedata['gameData']['teams']['home']['record']['wildCardGamesBack'];

    # MAY WANT TO ADD MORE DETAILS ABOUT RECORD HERE IN THE FUTURE...

    # venue

    venueId= gamedata['gameData']['venue']['id'];
    venueName= gamedata['gameData']['venue']['name'];
    venueLink= gamedata['gameData']['venue']['link'];
    venueLocationCity= gamedata['gameData']['venue']['location']['city'];
    if 'state' in gamedata['gameData']['venue']['location'].keys():
        venueLocationState= gamedata['gameData']['venue']['location']['state'];
        venueLocationStateAbbrev= gamedata['gameData']['venue']['location']['stateAbbrev'];
    else:
        venueLocationState= "";
        venueLocationStateAbbrev= "";
    # venueLocationLatitude= gamedata['gameData']['venue']['location']['defaultCoordinates']['latitude'];
    # venueLocationLongitude= gamedata['gameData']['venue']['location']['defaultCoordinates']['longitude'];

    # weather

    wxCondition= gamedata['gameData']['weather']['condition'];
    wxTemp= gamedata['gameData']['weather']['temp'];
    wxWind= gamedata['gameData']['weather']['wind'];

    # flags

    noHitter= gamedata['gameData']['flags']['noHitter'];
    perfectGame= gamedata['gameData']['flags']['perfectGame'];

    # boxscore

    # there might be some interesting stuff here... but for now I'm not too worried
    # about it. the most intersting is the team stats for the game, which, I presume
    # are the teams' stats up to this game. this might be interesting for some kinds
    # of matchup modeling.

    # linescore

    line_home_runs= gamedata['liveData']['linescore']['teams']['home']['runs'];
    line_home_hits= gamedata['liveData']['linescore']['teams']['home']['hits'];
    line_home_errors= gamedata['liveData']['linescore']['teams']['home']['errors'];
    line_home_lob= gamedata['liveData']['linescore']['teams']['home']['leftOnBase'];

    line_away_runs= gamedata['liveData']['linescore']['teams']['away']['runs'];
    line_away_hits= gamedata['liveData']['linescore']['teams']['away']['hits'];
    line_away_errors= gamedata['liveData']['linescore']['teams']['away']['errors'];
    line_away_lob= gamedata['liveData']['linescore']['teams']['away']['leftOnBase'];

    line_innings= gamedata['liveData']['linescore']['innings'];

    # accumulated runs
    inn_homeTotalRuns= 0;
    inn_homeTotalHits= 0;
    inn_homeTotalErrors= 0;
    inn_homeTotalLOB= 0;

    inn_awayTotalRuns= 0;
    inn_awayTotalHits= 0;
    inn_awayTotalErrors= 0;
    inn_awayTotalLOB= 0;

    for inning in line_innings:

        if 'runs' not in inning['away'].keys():
            break;

        inningNum= inning['num'];
        inningOrdinalNum= inning['ordinalNum'];

        inn_homeRuns= inning['home']['runs'] if 'runs' in inning['home'].keys() else 0;
        inn_homeHits= inning['home']['hits'];
        inn_homeErrors= inning['home']['errors'];
        inn_homeLOB= inning['home']['leftOnBase'];

        inn_awayRuns= inning['away']['runs'];
        inn_awayHits= inning['away']['hits'];
        inn_awayErrors= inning['away']['errors'];
        inn_awayLOB= inning['away']['leftOnBase'];

        # accumulate
        inn_homeTotalRuns += inn_homeRuns;
        inn_homeTotalHits += inn_homeHits;
        inn_homeTotalErrors += inn_homeErrors;
        inn_homeTotalLOB += inn_homeLOB;

        inn_awayTotalRuns += inn_awayRuns;
        inn_awayTotalHits += inn_awayHits;
        inn_awayTotalErrors += inn_awayErrors;
        inn_awayTotalLOB += inn_awayLOB;

        # bag it...
        output_inning= {
            'gamePk': gamePk,
            'inningNum': inningNum,
            'awayRuns': inn_awayRuns,
            'homeRuns': inn_homeRuns,
            'awayHits': inn_awayHits,
            'homeHits': inn_homeHits,
            'awayErrors': inn_awayErrors,
            'homeErrors': inn_homeErrors,
            'awayLOB': inn_awayLOB,
            'homeLOB': inn_homeLOB,
            'awayTotalRuns': inn_awayTotalRuns,
            'homeTotalRuns': inn_homeTotalRuns,
            'awayTotalHits': inn_awayTotalHits,
            'homeTotalHits': inn_homeTotalHits,
            'awayTotalErrors': inn_awayTotalErrors,
            'homeTotalErrors': inn_homeTotalErrors,
            'awayTotalLOB': inn_awayTotalLOB,
            'homeTotalLOB': inn_homeTotalLOB,
        };

        # and tag it...
        output_innings.append(output_inning);

    # decisions

    if 'winner' in gamedata['liveData']['decisions']:
        decide_winner_id= gamedata['liveData']['decisions']['winner']['id'];
        decide_winner_fullName= gamedata['liveData']['decisions']['winner']['fullName'];
    else:
        decide_winner_id= None;
        decide_winner_fullName= None;

    if 'loser' in gamedata['liveData']['decisions']:
        decide_loser_id= gamedata['liveData']['decisions']['loser']['id'];
        decide_loser_fullName= gamedata['liveData']['decisions']['loser']['fullName'];
    else:
        decide_loser_id= None;
        decide_loser_fullName= None;

    if 'save' in gamedata['liveData']['decisions']:
        decide_save_id= gamedata['liveData']['decisions']['save']['id'];
        decide_save_fullName= gamedata['liveData']['decisions']['save']['fullName'];
    else:
        decide_save_id= None;
        decide_save_fullName= None;

    # atbats

    play_types= {};

    inning_state= {'num': 1, 'istop': True};
    out_state= 0;
    base_state= {'1B': [], '2B': [], '3B': []};
    score_state= {'away': 0, 'home': 0};

    movementreason_list= {};

    for playNum in range(len(gamedata['liveData']['plays']['allPlays'])):

        if verbose:
            print("play no.: %d, game %d" % (playNum, gamePk,));

        play= gamedata['liveData']['plays']['allPlays'][playNum];

        # each entry is one raw plate appearance or onfield event

        play_type= play['result']['type'];
        play_event= play['result']['event'];
        play_eventType= play['result']['eventType'];
        play_description= play['result']['description'];
        play_rbi= play['result']['rbi'];

        play_index= play['about']['atBatIndex'];
        play_halfInning= play['about']['halfInning'];
        play_inning= play['about']['inning'];
        play_starttime= play['about']['startTime'];
        play_endtime= play['about']['endTime'];

        # quick interruption to update the inning state
        newinning= False;
        # print(play_halfInning);
        # print(inning_state);
        if ((play_inning != inning_state['num']) or
                (play_halfInning != ("top" if inning_state['istop'] else "bottom"))):
            newinning= True;

            if inning_state['istop']:

                # consistency check to validate the left-on-base numbers
                testlob= 0;
                for base in base_state:
                    testlob += len(base_state[base]);
                assert testlob == \
                        line_innings[inning_state['num'] - 1]['away']['leftOnBase'];

                inning_state['istop']= False;
            else:
                # consistency check to validate the left-on-base numbers
                testlob= 0;
                for base in base_state:
                    testlob += len(base_state[base]);
                assert testlob == \
                        line_innings[inning_state['num'] - 1]['home']['leftOnBase'];

                inning_state['istop']= True;
                inning_state['num'] += 1;
            out_state= 0;
            base_state_pre= base_state_empty();
        else:
            base_state_pre= base_state.copy();
        print("base_state before the play starts:");
        print(base_state_pre);
        base_state= base_state_pre;
        # and now back to your regularly scheduled programming

        play_startTime= play['about']['startTime'];
        play_endTime= play['about']['endTime'];
        play_iscomplete= play['about']['isComplete'];
        play_isscoring= play['about']['isScoringPlay'];
        play_hasreview= play['about']['hasReview'];
        play_hasout= play['about']['hasOut'];

        play_awayScore= play['result']['awayScore'];
        play_homeScore= play['result']['homeScore'];

        play_count= {'balls': play['count']['balls'],
                'strikes': play['count']['strikes'],
                'outs': play['count']['outs']};

        play_batterId= play['matchup']['batter']['id'];
        play_batterSide= play['matchup']['batSide']['code'];
        play_pitcherId= play['matchup']['pitcher']['id'];
        play_pitcherHand= play['matchup']['pitchHand']['code'];

        play_split_batter= play['matchup']['splits']['batter'];
        play_split_pitcher= play['matchup']['splits']['pitcher'];
        play_split_menonbase= play['matchup']['splits']['menOnBase'];

        play_pitch_indices= play['pitchIndex'];
        play_action_indices= play['actionIndex'];

        # runners as always... try to filter by movementReason and event to classify
        # pre-play, play, and post-play movement

        # don't think i actually need to do this... psych yes i did
        play_nplayevent= len(play['playEvents']);

        play_event_runners= []; # list for each event index of the runners for that event

        runner_map= [];
        for i in range(play_nplayevent):
            event_runners= [];
            for j in range(len(play['runners'])):
                runner= play['runners'][j];
                if runner['details']['playIndex'] == i:
                    event_runners.append(j);
                    runner_map.append(j);
            play_event_runners.append(event_runners);

        assert sorted(runner_map) == list(range(len(play['runners'])));

        out_state_preevent= None;
        out_state_postevent= None;
        base_state_preevent= None;
        base_state_postevent= None;
        score_state_preevent= None;
        score_state_postevent= None;

        runsscored_preevent= 0;
        runsscored_event= 0;
        runsscored_downstream= 0;

        play_rbi= 0;
        play_earnedRuns= 0;
        play_teamUnearnedRuns= 0;

        # loop over play-events
        for eventno in range(play_nplayevent):

            if verbose:
                print("eventno: %d\n" % eventno);

            # set up credit/debit accounts for this play event on base-out state
            event_base_state_add= base_state_empty();
            event_base_state_remove= base_state_empty();
            downstream_base_state_add= base_state_empty();
            downstream_base_state_remove= base_state_empty();
            thekeyevent= False;

            # get the playEvent of this number
            playevent= play['playEvents'][eventno];

            # if this is a pinch runner, swap out the runners accordingly
            if (playevent['type'] == "action") and \
                    (playevent['details']['eventType'] == "offensive_substitution") \
                    and (playevent['position']['abbreviation'] == "PR"):

                substitute= playevent['player']['id']; # incoming player
                base_sub= ['1B', '2B', '3B'][playevent['base'] - 1]; # base of substitute
                assert (len(base_state[base_sub]) == 1);
                # if verbose:
                #     print("%s should be substituted for %s at %s" % (substitute,
                #             base_state[base_sub][0], substitute));

                # remove the outgoing player and insert the incoming one
                event_base_state_remove[base_sub].append(base_state[base_sub][0]);
                event_base_state_add[base_sub].append(substitute);

            # if this is _the_ event of the PA, stash the pre-action numbers
            if (eventno == (play_nplayevent - 1)):
                out_state_preevent= out_state;
                base_state_preevent= base_state.copy();
                score_state_preevent= score_state.copy();
                thekeyevent= True;

                if runsscored_event > 0:
                    runsscored_preevent= runsscored_event;
                    runsscored_event= 0;

                # handle specially force on lead runner
                if (play_count['outs'] == 3) and \
                        (play_eventType == "force_out") and \
                        (len(base_state['1B']) == 1) and \
                        (len(base_state['2B']) == 1) and \
                        (len(base_state['3B']) == 1):

                    out_state += 1;

                    base_state_preevent= base_state.copy();
                    score_state_preevent= score_state.copy();

                    to1B= play_batterId;
                    to2B= base_state['1B'].pop();
                    to3B= base_state['2B'].pop();
                    toForce= base_state['3B'].pop();
                    base_state['1B'].append(to1B);
                    base_state['2B'].append(to2B);
                    base_state['3B'].append(to3B);

                    base_state_postevent= base_state.copy();
                    score_state_postevent= score_state.copy();

                    break;

                elif (play_count['outs'] == 3) and \
                        (play_eventType == "force_out") and \
                        (len(base_state['1B']) == 1) and \
                        (len(base_state['2B']) == 1):

                    out_state += 1;

                    base_state_preevent= base_state.copy();
                    score_state_preevent= score_state.copy();

                    to1B= play_batterId;
                    to2B= base_state['1B'].pop();
                    toForce= base_state['2B'].pop();
                    base_state['1B'].append(to1B);
                    base_state['2B'].append(to2B);

                    base_state_postevent= base_state.copy();
                    score_state_postevent= score_state.copy();

                    break;

                elif (play_count['outs'] == 3) and \
                        (play_eventType == "force_out") and \
                        (len(base_state['1B']) == 1):

                    out_state += 1;

                    base_state_preevent= base_state.copy();
                    score_state_preevent= score_state.copy();

                    to1B= play_batterId;
                    toForce= base_state['1B'].pop();
                    base_state['1B'].append(to1B);

                    base_state_postevent= base_state.copy();
                    score_state_postevent= score_state.copy();

                    break;

            out_log= [];
            # loop over runners in this play event
            for idx_runner in range(len(play_event_runners[eventno])):
                runner= play['runners'][play_event_runners[eventno][idx_runner]];

                # do the math
                (out2log, base_state_add, base_state_remove,
                        runner_scored, runner_rbi, runner_earnedRuns,
                        runner_teamUnearnedRuns, runner_eventType)= \
                        do_runner(runner, out_state);
                if runner['details']['movementReason'] == "r_interference":
                    base_state_add= base_state_empty();
                    base_state_remove= base_state_empty();
                    runner_scored= 0;
                    runner_rbi= 0;
                    runner_earnedRuns= 0;
                    runner_teamUnearnedRuns= 0;
                else:
                    out_log= out_log + out2log;

                # some accounting
                play_rbi += runner_rbi;
                play_earnedRuns += runner_earnedRuns;
                play_teamUnearnedRuns += runner_teamUnearnedRuns;

                # make sure it's working
                assert base_state_pre.keys() == base_state_add.keys() \
                        == base_state_remove.keys();

                # do credit/debit accounting

                print("runner_eventType: %s" % runner_eventType);
                print("play_eventType: %s" % play_eventType);
                print(base_state_remove);
                print(base_state_add);
                for base in base_state_add.keys():
                    for baserunner in base_state_add[base]:
                        if (not thekeyevent) or (runner_eventType == play_eventType):
                            event_base_state_add[base].append(baserunner);
                        else:
                            downstream_base_state_add[base].append(baserunner);
                for base in base_state_remove.keys():
                    for baserunner in base_state_remove[base]:
                        if (not thekeyevent) or (runner_eventType == play_eventType):
                            event_base_state_remove[base].append(baserunner);
                        else:
                            downstream_base_state_remove[base].append(baserunner);

                if (not thekeyevent) or (runner_eventType == play_eventType):
                    runsscored_event += runner_scored;
                else:
                    runsscored_downstream += runner_scored;

            out_state += len(set(out_log));

            # print("\t\t\trunsscored: preevent-%d, event-%d, downstream-%d" \
            #       % (runsscored_preevent, runsscored_event,
            #       runsscored_downstream));

            # add outs that are made (unique runners made out)
            # make sure additions and removals are unique
            for base in base_state.keys():
                event_base_state_remove[base]= list(set(event_base_state_remove[base]));
                event_base_state_add[base]= list(set(event_base_state_add[base]));
                downstream_base_state_remove[base]= list(set(downstream_base_state_remove[base]));
                downstream_base_state_add[base]= list(set(downstream_base_state_add[base]));

            # ok now let's file the base state event
            base_state= base_state_pre;
            print("base_state beginning event no. %d" % eventno);
            print(base_state);
            print(event_base_state_remove);
            print(event_base_state_add);
            print(downstream_base_state_remove);
            print(downstream_base_state_add);

            # work through the accounting for the through-event baserunners
            for base in base_state.keys():
                for baserunner in event_base_state_add[base]:
                    base_state[base].append(baserunner);

            print("base_state after adding runners (due event: %d)" % eventno);
            print(base_state);

            for base in base_state.keys():
                for baserunner in event_base_state_remove[base]:
                    if baserunner in base_state[base]:
                        base_state[base].remove(baserunner);

            print("base_state after removing runners (due event: %d)" % eventno);
            print(base_state);

            # stash base-state
            base_state_postevent= base_state.copy();

            # work through the accounting for the downstream baserunners
            for base in base_state.keys():
                for baserunner in downstream_base_state_add[base]:
                    base_state[base].append(baserunner);

            print("base_state after adding runners (after event: %d)" % eventno);
            print(base_state);

            for base in base_state.keys():
                for baserunner in downstream_base_state_remove[base]:
                    if baserunner in base_state[base]:
                        base_state[base].remove(baserunner);

            print("base_state after removing runners (after event: %d)" % eventno);
            print(base_state);

            # nix duplicates from erroneaous scorer recordings
            runners_onbase= [];
            for runner in base_state['3B']:
                if runner in runners_onbase:
                    base_state['3B'].remove(runner);
                else:
                    runners_onbase.append(runner);
            assert len(base_state['3B']) in [0, 1,];
            for runner in base_state['2B']:
                if runner in runners_onbase:
                    base_state['2B'].remove(runner);
                else:
                    runners_onbase.append(runner);
            assert len(base_state['2B']) in [0, 1,];
            for runner in base_state['1B']:
                if runner in runners_onbase:
                    base_state['1B'].remove(runner);
                else:
                    runners_onbase.append(runner);
            assert len(base_state['1B']) in [0, 1,];

            # copy into the next pre-state
            base_state_pre= base_state.copy();

            if verbose:
                print();

        # progress the score state accordingly, stashing the PA result
        score_state['away' if inning_state['istop'] else 'home'] \
                += runsscored_preevent;
        score_state_preevent= score_state.copy();
        score_state['away' if inning_state['istop'] else 'home'] \
                += runsscored_event;
        score_state_postevent= score_state.copy();
        score_state['away' if inning_state['istop'] else 'home'] \
                += runsscored_downstream;

        # output for debugging
        if verbose:
            print("\tinning: %s%d" % ("top" if inning_state['istop'] else "bottom",
                    inning_state['num']));
            print("\touts: %d" % out_state);
            print("\t1B: %d, 2B: %d, 3B: %d" % (len(base_state['1B']),
                    len(base_state['2B']), len(base_state['3B']),));
            print("\tscore: away- %d, home- %d" % (score_state['away'],
                    score_state['home']));
            print(base_state_preevent);
            print(base_state);
            print(base_state_postevent);

        # sanity-check- out state should be correct
        assert out_state == play_count['outs'];

        # sanity-check- one man per base for a valid base state
        for base in base_state.keys():
            assert len(base_state[base]) in [0, 1];
            assert len(base_state_preevent[base]) in [0, 1];
            assert len(base_state_postevent[base]) in [0, 1];

        # sanity-check- scores should be accounted for
        if inning_state['istop']:
            assert score_state['away'] == play_awayScore;
        else:
            assert score_state['home'] == play_homeScore;

        # sanity-check- make sure play no matches at-bat index (should always, i think)
        assert playNum == play['atBatIndex'];

        # some investigations of values
        if play_type not in play_types.keys():
            play_types[play_type]= 1;
        else:
            play_types[play_type] += 1;

        # signed, sealed, ...
        output_atbat= {
            'gamePk': gamePk,
            'playNum': playNum,
            'inningNum': play_inning,
            'inningHalf': 'T' if play_halfInning == 'top' else 'B',
            'batterId': play_batterId,
            'batterSide': play_batterSide,
            'pitcherId': play_pitcherId,
            'pitcherHand': play_pitcherHand,
            'awayScore': play_awayScore,
            'homeScore': play_homeScore,
            'awayScorePre': score_state_preevent['away'],
            'homeScorePre': score_state_preevent['home'],
            'b': play_count['balls'],
            's': play_count['strikes'],
            'o': play_count['outs'],
            'event': play_event,
            'eventType': play_eventType,
            'startTime': play_startTime,
            'endTime': play_endTime,
            'description': play_description,
            'oPre': out_state_preevent,
            'runsScored': runsscored_event,
            'rbi': play_rbi, # oops
            'earnedRuns': play_earnedRuns, # next
            'on1Bpre': (len(base_state_preevent['1B']) == 1),
            'on2Bpre': (len(base_state_preevent['2B']) == 1),
            'on3Bpre': (len(base_state_preevent['3B']) == 1),
            'on1Bpost': (len(base_state_postevent['1B']) == 1),
            'on2Bpost': (len(base_state_postevent['2B']) == 1),
            'on3Bpost': (len(base_state_postevent['3B']) == 1),
        };

        # and out for delivery.
        output_atbats.append(output_atbat);

        if verbose:
            print();

    # last but not least pack up the game-wise data
    output_game= {
        'gamePk': gamePk,
        'awayId': awayId,
        'homeId': homeId,
        'awayTeamName': awayTeamName,
        'homeTeamName': homeTeamName,
        'awayGamesPlayed': awayGamesPlayed,
        'homeGamesPlayed': homeGamesPlayed,
        'awayWins': awayWins,
        'homeWins': homeWins,
        'awayLosses': awayLosses,
        'homeLosses': homeLosses,
        'awayGB': awayGB if awayGB != "-" else 0,
        'homeGB': homeGB if homeGB != "-" else 0,
        'awayWCGB': awayWCGB if awayWCGB != "-" else 0,
        'homeWCGB': homeWCGB if homeWCGB != "-" else 0,
        'awayRuns': line_away_runs,
        'homeRuns': line_home_runs,
        'awayHits': line_away_hits,
        'homeHits': line_home_hits,
        'awayErrors': line_away_errors,
        'homeErrors': line_home_errors,
        'doubleHeader': doubleHeader,
        'dateTime': dateTime,
        'gameNumber': gameNumber,
        'gameType': gameType,
        'sportId': sportId,
        'venueId': venueId,
        'originalDate': originalDate,
        'statusCode': statusCode,
        'season': season,
        'wxCondition': wxCondition,
        'wxTemp': wxTemp,
        'wxWind': wxWind,
    }

    # print();
    # print("Ready to output to a database!");
    # print("\tgame %d processed!" % gamePk);
    # print();

    return (output_game, output_innings, output_atbats);

#
