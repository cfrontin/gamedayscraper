#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import json;
import urllib.request;

def generate_gamelist(season= 2020, sportId= 1, startDate= None, endDate= None,
        verbose= False, silent= False):

    if startDate is None:
        startDate= "%04d-01-01" % season;
    if endDate is None:
        endDate= "%04d-12-31" % season;

    assert str(season) == startDate[0:4], "season must match start date";
    assert str(season) == startDate[0:4], "season must match end date";

    game_list= [];

    # generate a url and get the data
    url_schedule= ("https://statsapi.mlb.com/api/v1/schedule?sportId=%d&"
            + "startDate=%s&endDate=%s") % (sportId, startDate, endDate);

    if verbose:
        print("\n generating games for:")
        print("\tseason: %d" % season);
        print("\tstartDate: %s" % startDate);
        print("\tendDate: %s" % endDate);
        print("\turl: %s" % url_schedule);

    with urllib.request.urlopen(url_schedule) as request:
        scheduledata= json.loads(request.read().decode());

    if verbose:
        print();

    for date in scheduledata['dates']:
        if verbose:
            print("\t%s" % date['date']);

        for game in date['games']:
            gamePk= game['gamePk'];
            game_list.append(gamePk);
            if verbose:
                print("\t\t%s" % gamePk);

    return game_list;
