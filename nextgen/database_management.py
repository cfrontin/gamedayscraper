#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import psycopg2;

def openGamedayDatabase(conn= None, database= "gameday_ng", host= "localhost", \
        verbose= False, silent= False):
    """
    a function to open a postgres database, and a connection, if it's not open
    already. otherwise, grabs a cursor from the one that already exists

    parameters
    ----------
    conn : psycopg2 connection
        psycopg2 connection, needing a cursor. defaults to None
    database : string
        database name
    host : string
        hostname
    verbose : boolean
        dump a ton of information?
    silent : boolean
        shut the hell up

    returns
    -------
    conn : psycopg2 connection
        a psycopg2 connection, alive and well
    conn.cursor() : psycopg2 cursor
        the cursor for the above connection
    """

    if conn is not None:
        return (conn, conn.cursor());

    conn= psycopg2.connect(database= database, host= host);

    if not silent:
        print();
        print("opened gameday database successfully.\n");

    return (conn, conn.cursor());

def closeGamedayDatabase(conn, verbose= False, silent= False):

    conn.close();

    if not silent:
        print("closed gameday database successfully.\n");

def createCustomTypes(conn= None, database= "gameday_ng", host= "localhost", \
        verbose= False, silent= False):
    """
    create custom enum types in the remote

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;
        (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
    else:
        curs= conn.cursor();

    str_execute= ('''
        DO $$ BEGIN
            CREATE TYPE halfinning AS ENUM('T', 'B');
        EXCEPTION
            WHEN duplicate_object THEN null;
        END $$;
        DO $$ BEGIN
            CREATE TYPE side AS ENUM('L', 'R');
        EXCEPTION
            WHEN duplicate_object THEN null;
        END $$;
    ''');
    curs.execute(str_execute);

    if not silent:
      print("\tcreated custom types successfully.");

    conn.commit();

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def createGameTable(conn= None, database= "gameday_ng", host= "localhost", \
        verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for games
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;
        (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
    else:
        curs= conn.cursor();

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS game
            (
                gamePk              char(6),
                awayId              char(3),
                homeId              char(3),
                awayTeamName        char(25),
                homeTeamName        char(25),
                awayGamesPlayed     smallint,
                homeGamesPlayed     smallint,
                awayWins            smallint,
                homeWins            smallint,
                awayLosses          smallint,
                homeLosses          smallint,
                awayGB              float,
                homeGB              float,
                awayWCGB            float,
                homeWCGB            float,
                awayRuns            smallint,
                homeRuns            smallint,
                awayHits            smallint,
                homeHits            smallint,
                awayErrors          smallint,
                homeErrors          smallint,
                awaySB              smallint,
                homeSB              smallint,
                awaySO              smallint,
                homeSO              smallint,
                doubleheader        char(1),
                dateTime            timestamp,
                gameNumber          smallint,
                gameType            char(1),
                sportId             char(3),
                venueId             char(4),
                originalDate        timestamp,
                statusCode          char(2),
                season              smallint,
                wxCondition         char(14),
                wxTemp              float,
                wxWind              char(20),
                PRIMARY KEY (gamePk)
            );
    ''');
    curs.execute(str_execute);

    if not silent:
      print("\tcreated games table successfully.");

    conn.commit();

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def createInningTable(conn= None, database= "gameday_ng", host= "localhost", \
        verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for innings
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;
        (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
    else:
        curs= conn.cursor();

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS inning
            (
                gamePk              char(6),
                inningNum           smallint,
                awayRuns            smallint,
                homeRuns            smallint,
                awayHits            smallint,
                homeHits            smallint,
                awayErrors          smallint,
                homeErrors          smallint,
                awayLOB             smallint,
                homeLOB             smallint,
                awayTotalRuns       smallint,
                homeTotalRuns       smallint,
                awayTotalHits       smallint,
                homeTotalHits       smallint,
                awayTotalErrors     smallint,
                homeTotalErrors     smallint,
                awayTotalLOB        smallint,
                homeTotalLOB        smallint,
                PRIMARY KEY (gamePk, inningNum)
            );
    ''');
    curs.execute(str_execute);

    if not silent:
        print("\tcreated innings table successfully.");

    conn.commit();

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def createAtbatTable(conn= None, database= "gameday_ng", host= "localhost", \
        verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for atbats
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;
        (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
    else:
        curs= conn.cursor();

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS atbat
            (
                gamePk              char(6),
                playNum             smallint,
                inningNum           smallint,
                inningHalf          halfinning,
                batterId            char(6),
                batterSide          side,
                pitcherId           char(6),
                pitcherHand         side,
                awayScore           smallint,
                homeScore           smallint,
                awayScorePre        smallint,
                homeScorePre        smallint,
                b                   smallint,
                s                   smallint,
                o                   smallint,
                event               char(50),
                eventType           char(28),
                startTime           timestamp,
                endTime             timestamp,
                description         text,
                oPre                smallint,
                runsScored          smallint,
                rbi                 smallint,
                earnedRuns          smallint,
                on1Bpre             boolean,
                on2Bpre             boolean,
                on3Bpre             boolean,
                on1Bpost            boolean,
                on2Bpost            boolean,
                on3Bpost            boolean,
                PRIMARY KEY (gamePk, playNum)
            );
    ''');

    curs.execute(str_execute);

    if not silent:
        print("\tcreated atbats table successfully.");

    conn.commit();

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

# def createRunnersTable(verbose= False, silent= False):
#     """
#     create a postgresql table in the gameday database for runners
#     if it doesn't already exist!
#
#     parameters
#     ----------
#     verbose : boolean
#         explicit?
#     silent : boolean
#         or quiet?
#     """
#
#     (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
#
#     str_execute= ('''
#         CREATE TABLE IF NOT EXISTS runner
#             (
#                 runner                  char(6),
#                 base_start              char(5),
#                 base_end                char(5),
#                 event                   char(50),
#                 score                   boolean,
#                 rbi                     boolean,
#                 earned                  boolean,
#                 inn_no                  smallint,
#                 inn_half                halfinning,
#                 gid                     char(30),
#                 numAB                   smallint,
#                 event_num               smallint,
#                 PRIMARY KEY (gid, inn_no, inn_half, numAB, event_num)
#             );
#     ''');
#
#     curs.execute(str_execute);
#
#     if not silent:
#         print("\tcreated runners table successfully.");
#
#     conn.commit();
#     conn.close();
#     if not silent:
#         print("closed gameday database successfully.\n");

# def createPitchesTable(verbose= False, silent= False):
#     """
#     create a postgresql table in the gameday database for pitches
#     if it doesn't already exist!
#
#     parameters
#     ----------
#     verbose : boolean
#         explicit?
#     silent : boolean
#         or quiet?
#     """
#
#     (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);
#
#     str_execute= ('''
#         CREATE TABLE IF NOT EXISTS pitch
#             (
#                 des                     text,
#                 pitch_id                smallint,
#                 type                    char(2),
#                 tfs                     char(6),
#                 tfs_zulu                char(24),
#                 x                       real,
#                 y                       real,
#                 start_speed             real,
#                 end_speed               real,
#                 pfx_x                   real,
#                 pfx_z                   real,
#                 px                      real,
#                 pz                      real,
#                 x0                      real,
#                 y0                      real,
#                 z0                      real,
#                 vx0                     real,
#                 vy0                     real,
#                 vz0                     real,
#                 ax                      real,
#                 ay                      real,
#                 az                      real,
#                 break_y                 real,
#                 break_angle             real,
#                 break_length            real,
#                 pitch_type              char(2),
#                 nasty                   real,
#                 spin_dir                real,
#                 spin_rate               real,
#                 inn_no                  smallint,
#                 inn_half                inninghalf,
#                 numAB                   smallint,
#                 on_1B                   char(6),
#                 on_2B                   char(6),
#                 on_3B                   char(6),
#                 count                   char(3),
#                 code                    char(2),
#                 play_guid               char(50),
#                 event_num               smallint,
#                 gid                     char(30),
#                 url                     text,
#                 PRIMARY KEY (gid, inn_no, inn_half, numAB, pitch_id)
#             );
#     ''');
#
#     curs.execute(str_execute);
#
#     if not silent:
#         print("\tcreated pitches table successfully.");
#
#     conn.commit();
#     conn.close();
#     if not silent:
#         print("closed gameday database successfully.\n");
