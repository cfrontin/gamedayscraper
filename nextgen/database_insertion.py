#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import psycopg2;

from database_management import *;

from utils import db_keys;

def table_upsert_command(tablename, keyvalue):
    """

    """

    (validate_keys, primary_keys)= db_keys(tablename);

    for key in validate_keys:
        assert key in keyvalue.keys();

    str_execute= "";
    str_execute += "INSERT INTO %s\n" % tablename;
    str_execute += "\t(\n";
    for (key, value) in keyvalue.items():
        str_execute += "\t\t%s,\n" % key;
    str_execute= str_execute[:-2]; # chop last two chars off
    str_execute += "\n\t)\n";

    str_execute += "VALUES\n";
    str_execute += "\t(\n";
    for (key, value) in keyvalue.items():
        value= str(value).replace("'", "");
        str_execute += "\t\t'%s',\n" % value;
    str_execute= str_execute[:-2]; # chop last two chars off
    str_execute += "\n\t)\n";

    str_execute += "ON CONFLICT (";
    for key in primary_keys:
        str_execute += "%s, " % key;
    str_execute= str_execute[:-2]; # chop last two chars off
    str_execute += ") DO UPDATE SET\n";
    for (key, value) in keyvalue.items():
        value= str(value).replace("'", "");
        str_execute += "\t%s= '%s',\n" % (key, value);
    str_execute= str_execute[:-2]; # chop last two chars off

    # print(str_execute);
    return str_execute;

def upsert_rows(conn, tablename, output_rows, verbose= False, silent= False):

    if type(output_rows) is dict:
        output_rows= [output_rows,];

    # get psycopg cursor
    curs= conn.cursor();

    N_count= 0;

    for output_row in output_rows:
        # generate and execute SQL query
        str_execute= table_upsert_command(tablename, output_row);
        curs.execute(str_execute);
        N_count += 1;

    conn.commit();

    if not silent:
        print("updated %d records in the %s table." % (N_count, tablename));

#
