#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import psycopg2;

from database_management import *;

def insertRunners(runners_data, conn= None, verbose= False, silent= False):
    """
    insert a row into the runners table of the gameday database with a new
    runner. if the runner already exists, update it!

    parameters
    ----------
    runners_data : dict
        dictionary of runners data in bs4 format
    conn : psycopg2 connection
        connection to recycle
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;

    (conn, curs)= openGamedayDatabase(conn= conn, verbose= verbose, \
            silent= silent);

    # loop over individual runners
    for runner_data in runners_data:

        str_execute= " \
            INSERT INTO runner \
                ( \
                    runner, \
                    base_start, \
                    base_end, \
                    event, \
                    score, \
                    rbi, \
                    earned, \
                    inn_no, \
                    inn_half, \
                    gid, \
                    numAB, \
                    event_num \
                ) \
            VALUES \
                ('" \
                    + runner_data['runner'] + "', '" \
                    + runner_data['base_start'] + "', '" \
                    + runner_data['base_end'] + "', '" \
                    + runner_data['event'] + "', '" \
                    + runner_data['score'] + "', '" \
                    + runner_data['rbi'] + "', '" \
                    + runner_data['earned'] + "', '" \
                    + str(runner_data['inn_no']) + "', '" \
                    + runner_data['inn_half'] + "', '" \
                    + runner_data['gid'] + "', '" \
                    + str(runner_data['numAB']) + "', '" \
                    + str(runner_data['event_num']) + "'" \
                ") \
            ON CONFLICT (gid, inn_no, inn_half, numAB, event_num) \
            DO \
                UPDATE \
                SET " \
                    + "runner= '" + runner_data['runner'] + "', " \
                    + "base_start= '" + runner_data['base_start'] + "', " \
                    + "base_end= '" + runner_data['base_end'] + "', " \
                    + "event= '" + runner_data['event'] + "', " \
                    + "score= '" + runner_data['score'] + "', " \
                    + "rbi= '" + runner_data['rbi'] + "', " \
                    + "earned= '" + runner_data['earned'] + "', " \
                    + "inn_no= '" + str(runner_data['inn_no']) + "', " \
                    + "inn_half= '" + runner_data['inn_half'] + "', " \
                    + "gid= '" + runner_data['gid'] + "', " \
                    + "numAB= '" + str(runner_data['numAB']) + "', " \
                    + "event_num= '" + str(runner_data['event_num']) + "'"

        # print(str_execute);

        curs.execute(str_execute);

    conn.commit();

    if verbose:
        print("\trunner records created successfully.");

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def insertAtbats(atbats_data, conn= None, verbose= False, silent= False):
    """
    insert a row into the atbats table of the gameday database with a new atbat.
    if the atbat already exists, update it!

    parameters
    ----------
    atbats_data : dict
        dictionary of atbat data in bs4 format
    conn : psycopg2 connection
        connection to recycle
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;

    (conn, curs)= openGamedayDatabase(conn= conn, verbose= verbose, \
            silent= silent);

    # loop over individual atbats
    for atbat_data in atbats_data:

        str_execute= "\
            INSERT INTO atbat \
                ( \
                    gid, \
                    inn_no, \
                    inn_half, \
                    numAB, \
                    batter, \
                    pitcher, \
                    b_height, \
                    away_team_runs, \
                    home_team_runs, \
                    away_runs_pre, \
                    home_runs_pre, \
                    b, \
                    s, \
                    o, \
                    event, \
                    event_num, \
                    p_throws, \
                    stand, \
                    start_tfs_zulu, \
                    end_tfs_zulu, \
                    play_guid, \
                    des, \
                    o_pre, \
                    runs_scored, \
                    rbi, \
                    earned_runs, \
                    on_1B_pre, \
                    on_2B_pre, \
                    on_3B_pre, \
                    on_1B_post, \
                    on_2B_post, \
                    on_3B_post \
                ) \
            VALUES \
                ('" \
                    + atbat_data['gid'] + "', '" \
                    + str(atbat_data['inn_no']) + "', '" \
                    + atbat_data['inn_half'] + "', '" \
                    + str(atbat_data['numAB']) + "', '" \
                    + atbat_data['batter'] + "', '" \
                    + atbat_data['pitcher'] + "', '" \
                    + str(atbat_data['b_height']) + "', '" \
                    + str(atbat_data['away_team_runs']) + "', '" \
                    + str(atbat_data['home_team_runs']) + "', '" \
                    + str(atbat_data['away_runs_pre']) + "', '" \
                    + str(atbat_data['home_runs_pre']) + "', '" \
                    + str(atbat_data['b']) + "', '" \
                    + str(atbat_data['s']) + "', '" \
                    + str(atbat_data['o']) + "', '" \
                    + atbat_data['event'] + "', '" \
                    + str(atbat_data['event_num']) + "', '" \
                    + atbat_data['p_throws'] + "', '" \
                    + atbat_data['stand'] + "', '" \
                    + atbat_data['start_tfs_zulu'] + "', '" \
                    + atbat_data['end_tfs_zulu'] + "', '" \
                    + atbat_data['play_guid'] + "', '" \
                    + atbat_data['des'] + "', '" \
                    + str(atbat_data['o_pre']) + "', '" \
                    + str(atbat_data['runs_scored']) + "', '" \
                    + str(atbat_data['rbi']) + "', '" \
                    + str(atbat_data['earned_runs']) + "', '" \
                    + atbat_data['on_1B_pre'] + "', '" \
                    + atbat_data['on_2B_pre'] + "', '" \
                    + atbat_data['on_3B_pre'] + "', '" \
                    + atbat_data['on_1B_post'] + "', '" \
                    + atbat_data['on_2B_post'] + "', '" \
                    + atbat_data['on_3B_post'] + "'" \
                ") \
            ON CONFLICT (gid, inn_no, inn_half, numAB) \
            DO \
                UPDATE \
                SET " \
                    + "gid= '" + atbat_data['gid'] + "', " \
                    + "inn_no= '" + str(atbat_data['inn_no']) + "', " \
                    + "inn_half= '" + atbat_data['inn_half'] + "', " \
                    + "numAB= '" + str(atbat_data['numAB']) + "', " \
                    + "batter= '" + atbat_data['batter'] + "', " \
                    + "pitcher= '" + atbat_data['pitcher'] + "', " \
                    + "b_height= '" + str(atbat_data['b_height']) + "', " \
                    + "away_team_runs= '" + str(atbat_data['away_team_runs']) + "', " \
                    + "home_team_runs= '" + str(atbat_data['home_team_runs']) + "', " \
                    + "away_runs_pre= '" + str(atbat_data['away_runs_pre']) + "', " \
                    + "home_runs_pre= '" + str(atbat_data['home_runs_pre']) + "', " \
                    + "b= '" + str(atbat_data['b']) + "', " \
                    + "s= '" + str(atbat_data['s']) + "', " \
                    + "o= '" + str(atbat_data['o']) + "', " \
                    + "event= '" + atbat_data['event'] + "', " \
                    + "event_num= '" + str(atbat_data['event_num']) + "', " \
                    + "p_throws= '" + atbat_data['p_throws'] + "', " \
                    + "stand= '" + atbat_data['stand'] + "', " \
                    + "start_tfs_zulu= '" + atbat_data['start_tfs_zulu'] + "', " \
                    + "end_tfs_zulu= '" + atbat_data['end_tfs_zulu'] + "', " \
                    + "play_guid= '" + atbat_data['play_guid'] + "', " \
                    + "des= '" + atbat_data['des'] + "', " \
                    + "o_pre= '" + str(atbat_data['o_pre']) + "', " \
                    + "runs_scored= '" + str(atbat_data['runs_scored']) + "', " \
                    + "rbi= '" + str(atbat_data['rbi']) + "', " \
                    + "earned_runs= '" + str(atbat_data['earned_runs']) + "', " \
                    + "on_1B_pre= '" + atbat_data['on_1B_pre'] + "', " \
                    + "on_2B_pre= '" + atbat_data['on_2B_pre'] + "', " \
                    + "on_3B_pre= '" + atbat_data['on_3B_pre'] + "', " \
                    + "on_1B_post= '" + atbat_data['on_1B_post'] + "', " \
                    + "on_2B_post= '" + atbat_data['on_2B_post'] + "', " \
                    + "on_3B_post= '" + atbat_data['on_3B_post'] + "'"

        # print(str_execute);

        curs.execute(str_execute);

        # conn.commit();

    conn.commit();

    if verbose:
        print("\tatbat records created successfully.");

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def insertInnings(game_data, conn= None, verbose= False, silent= False):
    """
    insert a row into the innings table of the gameday database with a new
    inning. if the inning already exists, update it!

    parameters
    ----------
    game_data : dict
        dictionary of game data in bs4 format
    conn : psycopg2 connection
        connection to recycle
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;

    (conn, curs)= openGamedayDatabase(conn= conn, verbose= verbose, \
            silent= silent);

    for inning in game_data['innings']:

        str_execute= " \
            INSERT INTO inning \
                ( \
                    gid, \
                    inn_no, \
                    away_runs, \
                    home_runs, \
                    away_score, \
                    home_score \
                ) \
            VALUES \
                ('" \
                    + 'gid_' + game_data['gid'] + "', '" \
                    + str(inning['inn_no']) + "', '" \
                    + str(inning['away_runs']) + "', '" \
                    + str(inning['home_runs']) + "', '" \
                    + str(inning['away_score']) + "', '" \
                    + str(inning['home_score']) + "'" \
                ") \
            ON CONFLICT (gid, inn_no) \
            DO \
                UPDATE \
                SET " \
                    + "gid= '" + 'gid_' + game_data['gid'] + "', " \
                    + "inn_no= '" + str(inning['inn_no']) + "', " \
                    + "away_runs= '" + str(inning['away_runs']) + "', " \
                    + "home_runs= '" + str(inning['home_runs']) + "', " \
                    + "away_score= '" + str(inning['away_score']) + "', " \
                    + "home_score= '" + str(inning['home_score']) + "'";

        curs.execute(str_execute);

    conn.commit();

    if verbose:
        print("\tinning records created successfully.");

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def insertGame(game_data, conn= None, verbose= False, silent= False):
    """
    insert a row into the games table of the gameday database with a new game.
    if the game already exists, update it!

    parameters
    ----------
    game_data : dict
        dictionary of game data in bs4 format
    conn : psycopg2 connection
        connection to recycle
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    disconnect= False;

    if conn is None:
        disconnect= True;

    (conn, curs)= openGamedayDatabase(conn= conn, verbose= verbose, \
            silent= silent);

    str_execute= "\
        INSERT INTO game \
            ( \
                gid, \
                away_code, \
                home_code, \
                away_name_abbrev, \
                home_name_abbrev, \
                away_division, \
                home_division, \
                away_league_id, \
                home_league_id, \
                away_games_back, \
                home_games_back, \
                away_games_back_wildcard, \
                home_games_back_wildcard, \
                away_loss, \
                home_loss, \
                away_win, \
                home_win, \
                away_sport_code, \
                home_sport_code, \
                away_team_id, \
                home_team_id, \
                away_team_name, \
                home_team_name, \
                away_team_city, \
                home_team_city, \
                away_time_zone, \
                home_time_zone, \
                away_runs, \
                home_runs, \
                away_hits, \
                home_hits, \
                away_errors, \
                home_errors, \
                away_sb, \
                home_sb, \
                away_so, \
                home_so, \
                double_header_sw, \
                first_pitch_et, \
                game_nbr, \
                game_type, \
                game_pk, \
                league, \
                location, \
                original_date, \
                resume_date, \
                scheduled_innings, \
                ser_games, \
                ser_home_nbr, \
                series, \
                series_num, \
                stats_season, \
                tbd_flag, \
                tiebreaker_sw, \
                time, \
                time_date, \
                ampm, \
                time_zone, \
                venue, \
                venue_id, \
                status_ind, \
                status_reason, \
                status \
            ) \
        VALUES \
            ('" \
                + 'gid_' + game_data['gid'] + "', '" \
                + game_data['away_code'] + "', '" \
                + game_data['home_code'] + "', '" \
                + game_data['away_name_abbrev'] + "', '" \
                + game_data['home_name_abbrev'] + "', '" \
                + game_data['away_division'] + "', '" \
                + game_data['home_division'] + "', '" \
                + game_data['away_league_id'] + "', '" \
                + game_data['home_league_id'] + "', '" \
                + game_data['away_games_back'] + "', '" \
                + game_data['home_games_back'] + "', '" \
                + game_data['away_games_back_wildcard'] + "', '" \
                + game_data['home_games_back_wildcard'] + "', '" \
                + game_data['away_loss'] + "', '" \
                + game_data['home_loss'] + "', '" \
                + game_data['away_win'] + "', '" \
                + game_data['home_win'] + "', '" \
                + game_data['away_sport_code'] + "', '" \
                + game_data['home_sport_code'] + "', '" \
                + game_data['away_team_id'] + "', '" \
                + game_data['home_team_id'] + "', '" \
                + game_data['away_team_name'] + "', '" \
                + game_data['home_team_name'] + "', '" \
                + game_data['away_team_city'] + "', '" \
                + game_data['home_team_city'] + "', '" \
                + game_data['away_time_zone'] + "', '" \
                + game_data['home_time_zone'] + "', '" \
                + str(game_data['away_runs']) + "', '" \
                + str(game_data['home_runs']) + "', '" \
                + str(game_data['away_hits']) + "', '" \
                + str(game_data['home_hits']) + "', '" \
                + str(game_data['away_errors']) + "', '" \
                + str(game_data['home_errors']) + "', '" \
                + str(game_data['away_sb']) + "', '" \
                + str(game_data['home_sb']) + "', '" \
                + str(game_data['away_so']) + "', '" \
                + str(game_data['home_so']) + "', '" \
                + str(game_data['double_header_sw']) + "', '" \
                + game_data['first_pitch_et'] + "', '" \
                + game_data['game_nbr'] + "', '" \
                + game_data['game_type'] + "', '" \
                + game_data['game_pk'] + "', '" \
                + game_data['league'] + "', '" \
                + game_data['location'] + "', '" \
                + game_data['original_date'] + "', '" \
                + game_data['resume_date'] + "', '" \
                + game_data['scheduled_innings'] + "', '" \
                + game_data['ser_games'] + "', '" \
                + game_data['ser_home_nbr'] + "', '" \
                + game_data['series'] + "', '" \
                + game_data['series_num'] + "', '" \
                + game_data['stats_season'] + "', '" \
                + str(game_data['tbd_flag']) + "', '" \
                + str(game_data['tiebreaker_sw']) + "', '" \
                + game_data['time'] + "', '" \
                + game_data['time_date'] + "', '" \
                + game_data['ampm'] + "', '" \
                + game_data['time_zone'] + "', '" \
                + game_data['venue'] + "', '" \
                + game_data['venue_id'] + "', '" \
                + game_data['status_ind'] + "', '" \
                + game_data['status_reason'] + "', '" \
                + game_data['status'] + "'" \
            ") \
        ON CONFLICT (gid) \
        DO \
            UPDATE \
            SET " \
                + "gid= '" + 'gid_' + game_data['gid'] + "', " \
                + "away_code= '" + game_data['away_code'] + "', " \
                + "home_code= '" + game_data['home_code'] + "', " \
                + "away_name_abbrev= '" + game_data['away_name_abbrev'] + "', " \
                + "home_name_abbrev= '" + game_data['home_name_abbrev'] + "', " \
                + "away_division= '" + game_data['away_division'] + "', " \
                + "home_division= '" + game_data['home_division'] + "', " \
                + "away_league_id= '" + game_data['away_league_id'] + "', " \
                + "home_league_id= '" + game_data['home_league_id'] + "', " \
                + "away_games_back= '" + game_data['away_games_back'] + "', " \
                + "home_games_back= '" + game_data['home_games_back'] + "', " \
                + "away_games_back_wildcard= '" + game_data['away_games_back_wildcard'] + "', " \
                + "home_games_back_wildcard= '" + game_data['home_games_back_wildcard'] + "', " \
                + "away_loss= '" + game_data['away_loss'] + "', " \
                + "home_loss= '" + game_data['home_loss'] + "', " \
                + "away_win= '" + game_data['away_win'] + "', " \
                + "home_win= '" + game_data['home_win'] + "', " \
                + "away_sport_code= '" + game_data['away_sport_code'] + "', " \
                + "home_sport_code= '" + game_data['home_sport_code'] + "', " \
                + "away_team_id= '" + game_data['away_team_id'] + "', " \
                + "home_team_id= '" + game_data['home_team_id'] + "', " \
                + "away_team_name= '" + game_data['away_team_name'] + "', " \
                + "home_team_name= '" + game_data['home_team_name'] + "', " \
                + "away_team_city= '" + game_data['away_team_city'] + "', " \
                + "home_team_city= '" + game_data['home_team_city'] + "', " \
                + "away_time_zone= '" + game_data['away_time_zone'] + "', " \
                + "home_time_zone= '" + game_data['home_time_zone'] + "', " \
                + "away_runs= '" + str(game_data['away_runs']) + "', " \
                + "home_runs= '" + str(game_data['home_runs']) + "', " \
                + "away_hits= '" + str(game_data['away_hits']) + "', " \
                + "home_hits= '" + str(game_data['home_hits']) + "', " \
                + "away_errors= '" + str(game_data['away_errors']) + "', " \
                + "home_errors= '" + str(game_data['home_errors']) + "', " \
                + "away_sb= '" + str(game_data['away_sb']) + "', " \
                + "home_sb= '" + str(game_data['home_sb']) + "', " \
                + "away_so= '" + str(game_data['away_so']) + "', " \
                + "home_so= '" + str(game_data['home_so']) + "', " \
                + "double_header_sw= '" + str(game_data['double_header_sw']) + "', " \
                + "first_pitch_et= '" + game_data['first_pitch_et'] + "', " \
                + "game_nbr= '" + game_data['game_nbr'] + "', " \
                + "game_type= '" + game_data['game_type'] + "', " \
                + "game_pk= '" + game_data['game_pk'] + "', " \
                + "league= '" + game_data['league'] + "', " \
                + "location= '" + game_data['location'] + "', " \
                + "original_date= '" + game_data['original_date'] + "', " \
                + "resume_date= '" + game_data['resume_date'] + "', " \
                + "scheduled_innings= '" + game_data['scheduled_innings'] + "', " \
                + "ser_games= '" + game_data['ser_games'] + "', " \
                + "ser_home_nbr= '" + game_data['ser_home_nbr'] + "', " \
                + "series= '" + game_data['series'] + "', " \
                + "series_num= '" + game_data['series_num'] + "', " \
                + "stats_season= '" + game_data['stats_season'] + "', " \
                + "tbd_flag= '" + str(game_data['tbd_flag']) + "', " \
                + "tiebreaker_sw= '" + str(game_data['tiebreaker_sw']) + "', " \
                + "time= '" + game_data['time'] + "', " \
                + "time_date= '" + game_data['time_date'] + "', " \
                + "ampm= '" + game_data['ampm'] + "', " \
                + "time_zone= '" + game_data['time_zone'] + "', " \
                + "venue= '" + game_data['venue'] + "', " \
                + "venue_id= '" + game_data['venue_id'] + "', " \
                + "status_ind= '" + game_data['status_ind'] + "', " \
                + "status_reason= '" + game_data['status_reason'] + "', " \
                + "status= '" + game_data['status'] + "'";

    # print(str_execute);

    try:
        curs.execute(str_execute);
    except Exception as e:
        print(str_execute);
        raise e;

    conn.commit();

    if verbose:
      print("\tgame records created successfully.");

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");
