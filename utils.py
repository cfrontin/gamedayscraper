#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import requests;
import urllib2;
from bs4 import BeautifulSoup;

def soupify(url_soup, encoding= "lxml", backend= "urllib2", verbose= False):
    """
    a function to take a URL and get the bs4 parse of it, given settings

    parameters
    ----------
    url_soup : string
        the url to be parsed
    encoding : string
        the encoding method to be passed to bs4
    backend : string
        the url request library to use, either "requests" or "urllib2"
    verbose : boolean
        should it print a ton of explicit descriptions about what it's doing

    returns
    -------
    soup : dict
        dictionary of parsed data from bs4
    """

    if backend == "requests":
        req= requests.get(url_soup);
        data= req.text;
    elif backend == "urllib2":
        response= urllib2.urlopen(url_soup);
        data= response.read();
        if verbose:
            print("urllib2 response: " + response);
        response.close();
    else:
        raise ImplementationException;

    soup= BeautifulSoup(data, encoding);

    return soup;
