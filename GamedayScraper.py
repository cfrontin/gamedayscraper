#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import pprint;

from utils import soupify;

from database_management import *;

from scraping import *;

print("\nI'm learning how to scrape a gameday database.");

print("\n");
createCustomTypes(verbose= True);
createGamesTable(verbose= True);
createInningsTable(verbose= True);
createAtbatsTable(verbose= True);
createRunnersTable(verbose= True);
print("\n");

url_base= "http://gd2.mlb.com/components/game/mlb/";

# scrape_gameday(url_base, verbose= True);
# scrape_gameday(url_base, yearspec= (2019, 2019),
#                verbose= False, silent= False);
scrape_gameday(url_base, yearspec= (2019, 2019), monthspec= 8,
               verbose= False, silent= False);

print("\n");
