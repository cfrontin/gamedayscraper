#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import traceback;

from utils import soupify;

def parseGameData(url_gid, logfile= None, verbose= False, silent= False):
    """
    shred a innings_all.xml file, turning it into a game

    parameters
    ----------
    url_gid : string
        the url for the gid of interest
    logfile : filestream
        filestream to write a logfile to
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?

    returns
    -------
    outdata : dict
        dictionary of game data for a single game
    """

    gid= url_gid.strip('/').split('/')[-1];

    url_inningall= url_gid.strip('/') + "/inning/inning_all.xml";

    # req_game= requests.get(url_inningall);
    # data_game= req_game.text;
    # soup_game= BeautifulSoup(data_game, "lxml");

    soup_game= soupify(url_inningall);

    soup_game= soup_game.game;

    # print(soup_game.prettify());
    # print("\n");

    outdata= {};

    outdata['atbat']= [];
    # outdata['pitch']= [];             # a man can dream
    outdata['runner']= [];            # a man can dream

    away_runs_pre= 0;
    home_runs_pre= 0;

    if soup_game is not None: # in case of cancellation, etc.

        for soup_inning in soup_game.find_all('inning'):

            inn_no= int(soup_inning['num']);                # inning

            # print("inning %d found." % int(soup_inning['num']));

            o_pre= 0;

            baseToRunners= {};
            baseToRunners['1B']= [];
            baseToRunners['2B']= [];
            baseToRunners['3B']= [];

            baseToRunners_pre= baseToRunners.copy();

            pinches_waiting= [];

            for soup_atbat in soup_inning.find_all(['atbat', 'action']):

                # gotta check for offensive substitions:
                if soup_atbat.name == 'action':
                    if soup_atbat['event'] == "Offensive Substitution":
                        pinches_waiting.append(soup_atbat['player']);
                    continue;

                # grab that data!
                inn_half= '';                             # top or bottom?
                numAB= int(soup_atbat['num']);            # identifier
                batter= soup_atbat['batter'];             # in the box, MLBAM ID
                pitcher= soup_atbat['pitcher'];           # on the mound, MLBAM ID
                b_height= soup_atbat['b_height'];         # batter height "%d'%d"
                away_team_runs= int(soup_atbat['away_team_runs']);
                home_team_runs= int(soup_atbat['home_team_runs']);
                b= int(soup_atbat['b']);                  # balls
                s= int(soup_atbat['s']);                  # strikes
                o= int(soup_atbat['o']);                  # outs
                event= soup_atbat['event'];               # what happened
                event_num= int(soup_atbat['event_num']);  # the above, but a code
                p_throws= soup_atbat['p_throws'];         # southpaw or nah
                stand= soup_atbat['stand'];               # choose one
                start_tfs_zulu= soup_atbat['start_tfs_zulu']; # beginning of atbat
                end_tfs_zulu= soup_atbat['end_tfs_zulu']; # end of atbat
                try:
                    play_guid= soup_atbat['play_guid'];   # ???
                except:
                    play_guid= "";
                des= soup_atbat['des'];                   # text description

                # grab the parent to see whether its top or bottom of the inning
                if soup_atbat.parent.name == "top":
                    inn_half= 'T';
                else:
                    inn_half= 'B';

                # process batter height
                feet= int(b_height.split('\'')[0]);
                inches= int(b_height.split('\'')[1]);
                b_height= feet*12 + inches;

                # clean out description strings
                des= des.replace("'", "");
                des= des.replace('"', '');

                # print("atbat %03d found: %s of inning %d" \
                #         % (int(soup_atbat['num']), inn_half, inn_no));

                # # for future use:
                # for soup_pitch in soup_atbat.find_all('pitch'):
                #
                #     # grab that data!
                #     # cc= soup_pitch['cc'];                     # apparently unused
                #     code= soup_pitch['code'];                   # ball strike contact
                #     des= soup_pitch['des'];                     # description
                #     event_num= int(soup_pitch['event_num']);    # what happened, a code
                #     id= int(soup_pitch['id']);                  # identifier
                #     # mt= soup_pitch['mt'];                     # apparently unused?
                #     sz_bot= soup_pitch['sz_bot'];               # strikezone bottom
                #     sz_top= soup_pitch['sz_top'];               # strikezone top
                #     tfs= soup_pitch['tfs'];                     # timestamp
                #     tfs_zulu= soup_pitch['tfs_zulu'];           # full timestamp
                #     type= soup_pitch['type'];                   # outcome type
                #     x= soup_pitch['x'];                         # plate crossing?
                #     y= soup_pitch['y'];                         # plate crossing?

                runsScored= 0;
                RBI= 0;
                earnedRuns= 0;

                baseToRunners_pre= baseToRunners.copy();

                on1Bpre= 'T' if (len(baseToRunners['1B']) > 0) else 'F';
                on2Bpre= 'T' if (len(baseToRunners['2B']) > 0) else 'F';
                on3Bpre= 'T' if (len(baseToRunners['3B']) > 0) else 'F';

                on1Bpost= 'F';
                on2Bpost= 'F';
                on3Bpost= 'F';

                runnersData= [];

                baseToAddRunners_pre= {'1B': [], '2B': [], '3B': []};
                baseToSubRunners_pre= {'1B': [], '2B': [], '3B': []};
                baseToAddRunners_play= {'1B': [], '2B': [], '3B': []};
                baseToSubRunners_play= {'1B': [], '2B': [], '3B': []};
                baseToAddRunners_post= {'1B': [], '2B': [], '3B': []};
                baseToSubRunners_post= {'1B': [], '2B': [], '3B': []};

                apply_runner= False;
                after_play= False;

                pinch_dict= {};

                for soup_runner in soup_atbat.find_all('runner'):

                    runner= soup_runner['id'];
                    base_start= soup_runner['start'];
                    base_end= soup_runner['end'];
                    runner_event= soup_runner['event'];

                    ### check to see if the runner is from the pinch list,
                    if runner in pinches_waiting:
                        remove= False;
                        if base_start == '1B':
                            remove= True;
                            pinch_dict[runner].append(1);
                        elif base_start == '2B':
                            remove= True;
                            pinch_dict[runner].append(2);
                        elif base_start == '3B':
                            remove= True;
                            pinch_dict[runner].append(3);
                        if remove:
                            pinches_waiting.remove(runner);

                    ### create a list of pinch runner origin bases, remove
                    ### leftovers at the end of the AB one-per-pinch-originating
                    ### from the bases of interest

                    if (runner_event == event):
                        apply_runner= True;
                        after_play= True;

                    if 'score' in soup_runner:
                        score= soup_runner['score'];
                    else:
                        score= 'F';

                    if 'rbi' in soup_runner:
                        rbi= soup_runner['rbi'];
                    else:
                        rbi= 'F';

                    if 'earned' in soup_runner:
                        earned= soup_runner['earned'];
                    else:
                        earned= 'F';

                    pack_run= {};
                    pack_run['runner']= runner;
                    pack_run['base_start']= base_start;
                    pack_run['base_end']= base_end;
                    pack_run['event']= runner_event;
                    pack_run['score']= score;
                    pack_run['rbi']= rbi;
                    pack_run['earned']= earned;
                    # pack_run['url']= url;
                    pack_run['inn_no']= inn_no;
                    pack_run['inn_half']= inn_half;
                    pack_run['gid']= gid;
                    pack_run['numAB']= numAB;
                    pack_run['event_num']= event_num;

                    runnersData.append(pack_run);

                    # the base that the runner starts on
                    # print(baseToRunners);
                    if (base_start != ""):
                        if apply_runner:
                            baseToSubRunners_play[base_start].append(runner);
                        elif after_play or runner_event in ["Error", "Runner Out"]:
                            baseToSubRunners_post[base_start].append(runner);
                        else:
                            baseToSubRunners_pre[base_start].append(runner);

                    # the base that the runner ends on
                    if (base_end != "" and base_end != "score"):
                        if apply_runner:
                            baseToAddRunners_play[base_end].append(runner);
                        elif after_play or runner_event in ["Error", "Runner Out"]:
                            baseToAddRunners_post[base_end].append(runner);
                        else:
                            baseToAddRunners_pre[base_end].append(runner);

                baseToRunners_pre= baseToRunners.copy();

                try:
                    # account for pre-play action
                    for key in baseToAddRunners_pre.keys():
                        for runner in baseToAddRunners_pre[key]:
                            baseToRunners[key].append(runner);
                    for key in baseToSubRunners_pre.keys():
                        for runner in baseToSubRunners_pre[key]:
                            baseToRunners[key].remove(runner);

                    on1Bpre= 'T' if (len(baseToRunners['1B']) > 0) else 'F';
                    on2Bpre= 'T' if (len(baseToRunners['2B']) > 0) else 'F';
                    on3Bpre= 'T' if (len(baseToRunners['3B']) > 0) else 'F';

                    # account for-play action
                    for key in baseToAddRunners_play.keys():
                        for runner in baseToAddRunners_play[key]:
                            baseToRunners[key].append(runner);
                    for key in baseToSubRunners_play.keys():
                        for runner in baseToSubRunners_play[key]:
                            baseToRunners[key].remove(runner);

                    on1Bpost= 'T' if (len(baseToRunners['1B']) > 0) else 'F';
                    on2Bpost= 'T' if (len(baseToRunners['2B']) > 0) else 'F';
                    on3Bpost= 'T' if (len(baseToRunners['3B']) > 0) else 'F';

                    # account for post-play action
                    for key in baseToAddRunners_post.keys():
                        for runner in baseToAddRunners_post[key]:
                            baseToRunners[key].append(runner);
                    for key in baseToSubRunners_post.keys():
                        for runner in baseToSubRunners_post[key]:
                            baseToRunners[key].remove(runner);

                    assert (len(baseToRunners['1B']) in [0, 1]);
                    assert (len(baseToRunners['2B']) in [0, 1]);
                    assert (len(baseToRunners['3B']) in [0, 1]);

                except (ValueError, AssertionError):
                    traceback.print_exc();
                    
                    if logfile is not None:
                        logfile.write("\t\t\trunner error. gid- %s, numAB- %d\n" % (gid, numAB));
                    print("\t\trunner error. %s. gid- %s, numAB- %d\n" % (event, gid, numAB));
                    print("pinch_dict:");
                    print(pinch_dict);
                    print("baseToRunners_pre:");
                    print(baseToRunners_pre);
                    print("baseToAddRunners_pre:");
                    print(baseToAddRunners_pre);
                    print("baseToSubRunners_pre:");
                    print(baseToSubRunners_pre);
                    print("baseToAddRunners_play:");
                    print(baseToAddRunners_play);
                    print("baseToSubRunners_play:");
                    print(baseToSubRunners_play);
                    print("baseToAddRunners_post:");
                    print(baseToAddRunners_post);
                    print("baseToSubRunners_post:");
                    print(baseToSubRunners_post);

                pack_AB= {'gid': gid, 'inn_no': inn_no, 'inn_half': inn_half, \
                        'numAB': numAB, 'batter': batter, 'pitcher': pitcher, \
                        'b_height': b_height, 'away_team_runs': away_team_runs, \
                        'home_team_runs': home_team_runs, 'b': b, 's': s, 'o': o, \
                        'event': event, 'event_num': event_num, \
                        'p_throws': p_throws, 'stand': stand, \
                        'start_tfs_zulu': start_tfs_zulu, \
                        'end_tfs_zulu': end_tfs_zulu, 'play_guid': play_guid, \
                        'des': des, 'o_pre': o_pre, 'away_runs_pre': away_runs_pre, \
                        'home_runs_pre': home_runs_pre, 'runs_scored': runsScored, \
                        'rbi': RBI, 'earned_runs': earnedRuns,
                        'on_1B_pre': on1Bpre, 'on_2B_pre': on2Bpre, 'on_3B_pre': on3Bpre, \
                        'on_1B_post': on1Bpost, 'on_2B_post': on2Bpost, 'on_3B_post': on3Bpost};

                outdata['atbat'].append(pack_AB);
                outdata['runner'].append(runnersData);

                # pprint.pprint(pack_AB);

                o_pre= o;
                if o_pre == 3:
                    o_pre= 0;

                    baseToRunners= {};
                    baseToRunners['1B']= [];
                    baseToRunners['2B']= [];
                    baseToRunners['3B']= [];
                    baseToRunners_pre= baseToRunners.copy();

                away_runs_pre= away_team_runs;
                home_runs_pre= home_team_runs;

    return outdata;

def parseMasterScoreboardData(url_sb, verbose= False, silent= False):
    """
    shred a masterscoreboard.json file, turning it into game metadata for a
    given day

    parameters
    ----------
    url_sb : string
        the url for the masterscoreboard.json on one day
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?

    returns
    -------
    games_data : dict
        dictionary of game metadata for a single day
    """

    if verbose:
      print("recieved url: " + url_sb);

    # # pull request and parse as text
    # req= requests.get(url_sb);
    # data= req.text;
    #
    # # parse with bs
    # soup= BeautifulSoup(data, "lxml");

    soup= soupify(url_sb);

    # initialize list of dicts
    games_data= [];

    # for each game in the database
    for game in soup.find_all('game'):

        # initialize game data
        game_data= {};

        # set in data
        game_data['gid']= game['gameday'];
        game_data['away_code']= game['away_code'];
        game_data['home_code']= game['home_code'];
        game_data['away_name_abbrev']= game['away_name_abbrev'];
        game_data['home_name_abbrev']= game['home_name_abbrev'];
        game_data['away_division']= game['away_division'];
        game_data['home_division']= game['home_division'];
        game_data['away_league_id']= game['away_league_id'];
        game_data['home_league_id']= game['home_league_id'];
        game_data['away_games_back']= game['home_games_back'];
        game_data['home_games_back']= game['away_games_back'];
        game_data['away_games_back_wildcard']= game['away_games_back_wildcard'];
        game_data['home_games_back_wildcard']= game['home_games_back_wildcard'];
        game_data['away_loss']= game['away_loss'];
        game_data['home_loss']= game['home_loss'];
        game_data['away_win']= game['away_win'];
        game_data['home_win']= game['home_win'];
        game_data['away_sport_code']= game['away_sport_code'];
        game_data['home_sport_code']= game['home_sport_code'];
        game_data['away_team_id']= game['away_team_id'];
        game_data['home_team_id']= game['home_team_id'];
        game_data['away_team_name']= game['away_team_name'];
        game_data['home_team_name']= game['home_team_name'];
        game_data['away_team_city']= game['away_team_city'];
        game_data['home_team_city']= game['home_team_city'];
        game_data['away_time_zone']= game['away_time_zone'];
        game_data['home_time_zone']= game['home_time_zone'];
        game_data['away_runs']= game.linescore.r['away'];
        game_data['home_runs']= game.linescore.r['home'];
        game_data['away_hits']= game.linescore.h['away'];
        game_data['home_hits']= game.linescore.h['home'];
        game_data['away_errors']= game.linescore.e['away'];
        game_data['home_errors']= game.linescore.e['home'];
        game_data['away_sb']= game.linescore.sb['away'];
        game_data['home_sb']= game.linescore.sb['home'];
        game_data['away_so']= game.linescore.so['away'];
        game_data['home_so']= game.linescore.so['home'];
        game_data['double_header_sw']= game['double_header_sw'];
        game_data['first_pitch_et']= game['first_pitch_et'];
        game_data['game_nbr']= game['game_nbr'];
        game_data['game_type']= game['game_type'];
        game_data['game_pk']= game['game_pk'];
        game_data['league']= game['league'];
        game_data['location']= game['location'];
        game_data['original_date']= game['original_date'];
        game_data['resume_date']= game['resume_date'];
        game_data['scheduled_innings']= game['scheduled_innings'];
        game_data['ser_games']= game['ser_games'];
        game_data['ser_home_nbr']= game['ser_home_nbr'];
        game_data['series']= game['series'];
        game_data['series_num']= game['series_num'];
        game_data['stats_season']= game['stats_season'];
        game_data['tbd_flag']= game['tbd_flag'];
        game_data['tiebreaker_sw']= game['tiebreaker_sw'];
        game_data['time']= game['time'];
        game_data['time_date']= game['time_date'];
        game_data['ampm']= game['ampm'];
        game_data['time_zone']= game['time_zone'];
        game_data['venue']= game['venue'];
        game_data['venue_id']= str(game['venue_id']);
        game_data['status_ind']= game.status['ind'];
        game_data['status_reason']= game.status['reason'];
        game_data['status']= game.status['status'];

        # now parse the innings data
        game_data['innings']= [];

        # score through the innings so far...
        inningNo= 0;
        away_score= 0;
        home_score= 0;

        for inning in game.linescore.find_all('inning'):

            # extract the runs scored this inning
            away_runs= int('0' + inning['away']); # adding the zero fixes the
            home_runs= int('0' + inning['home']); #   empty string case!

            # increment to have the correct current score, inning number
            inningNo= inningNo + 1;
            away_score= away_score + away_runs;
            home_score= home_score + home_runs;

            game_data['innings'].append({'inn_no': inningNo,
                                         'away_runs': away_runs,
                                         'home_runs': home_runs,
                                         'away_score': away_score,
                                         'home_score': home_score});

        if verbose:
          # print gid added!
          print(game['gameday']);

        # add to the list
        games_data.append(game_data);

    return games_data;

def cleanupGame(game_data):
    """
    take game data and clean it up from scrape format to a better one for
    storage

    parameters
    ----------
    game_data : dict
        dictionary of game data in bs4 format
    """

    if game_data['away_runs'] == '':
        game_data['away_runs']= 0;
    if game_data['home_runs'] == '':
        game_data['home_runs']= 0;
    if game_data['away_hits'] == '':
        game_data['away_hits']= 0;
    if game_data['home_hits'] == '':
        game_data['home_hits']= 0;
    if game_data['away_errors'] == '':
        game_data['away_errors']= 0;
    if game_data['home_errors'] == '':
        game_data['home_errors']= 0;
    if game_data['away_sb'] == '':
        game_data['away_sb']= 0;
    if game_data['home_sb'] == '':
        game_data['home_sb']= 0;
    if game_data['away_so'] == '':
        game_data['away_so']= 0;
    if game_data['home_so'] == '':
        game_data['home_so']= 0;
    if game_data['away_games_back'] == '':
        game_data['away_games_back']= '0';
    if game_data['home_games_back'] == '':
        game_data['home_games_back']= '0';
    if game_data['away_games_back_wildcard'] == '':
        game_data['away_games_back_wildcard']= '0';
    if game_data['home_games_back_wildcard'] == '':
        game_data['home_games_back_wildcard']= '0';
    if game_data['ser_games'] == '':
        game_data['ser_games']= '0';
    if game_data['ser_home_nbr'] == '':
        game_data['ser_home_nbr']= '0';
    if game_data['series_num'] == '':
        game_data['series_num']= '0';
