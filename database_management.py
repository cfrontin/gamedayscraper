#!/usr/local/bin/python

from __future__ import print_function;
from __future__ import unicode_literals;
from __future__ import division;
from __future__ import absolute_import;

import psycopg2;

def openGamedayDatabase(conn= None, database= "gameday", host= "localhost", \
        verbose= False, silent= False):
    """
    a function to open a postgres database, and a connection, if it's not open
    already. otherwise, grabs a cursor from the one that already exists

    parameters
    ----------
    conn : psycopg2 connection
        psycopg2 connection, needing a cursor. defaults to None
    database : string
        database name
    host : string
        hostname
    verbose : boolean
        dump a ton of information?
    silent : boolean
        shut the hell up

    returns
    -------
    conn : psycopg2 connection
        a psycopg2 connection, alive and well
    conn.cursor() : psycopg2 cursor
        the cursor for the above connection
    """

    if conn is not None:
        return (conn, conn.cursor());

    conn= psycopg2.connect(database= database, host= host);

    if not silent:
        print("opened gameday database successfully.");

    return (conn, conn.cursor());

def createCustomTypes(verbose= False, silent= False):
    """
    create custom enum types in the remote

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        DO $$ BEGIN
            CREATE TYPE halfinning AS ENUM('T', 'B');
        EXCEPTION
            WHEN duplicate_object THEN null;
        END $$;
        DO $$ BEGIN
            CREATE TYPE side AS ENUM('L', 'R');
        EXCEPTION
            WHEN duplicate_object THEN null;
        END $$;
    ''');
    curs.execute(str_execute);

    if not silent:
      print("\tcreated custom types successfully.");

    conn.commit();
    conn.close();
    if not silent:
        print("closed gameday database successfully.\n");

def createGamesTable(verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for games
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS game
            (
                gid                         char(30),
                away_code                   char(3),
                home_code                   char(3),
                away_name_abbrev            char(3),
                home_name_abbrev            char(3),
                away_division               char(1),
                home_division               char(1),
                away_league_id              char(3),
                home_league_id              char(3),
                away_games_back             smallint,
                home_games_back             smallint,
                away_games_back_wildcard    smallint,
                home_games_back_wildcard    smallint,
                away_loss                   smallint,
                home_loss                   smallint,
                away_win                    smallint,
                home_win                    smallint,
                away_sport_code             char(3),
                home_sport_code             char(3),
                away_team_id                char(4),
                home_team_id                char(4),
                away_team_name              char(25),
                home_team_name              char(25),
                away_team_city              char(50),
                home_team_city              char(50),
                away_time_zone              char(3),
                home_time_zone              char(3),
                away_runs                   smallint,
                home_runs                   smallint,
                away_hits                   smallint,
                home_hits                   smallint,
                away_errors                 smallint,
                home_errors                 smallint,
                away_sb                     smallint,
                home_sb                     smallint,
                away_so                     smallint,
                home_so                     smallint,
                double_header_sw            char(3),
                first_pitch_et              char(5),
                game_nbr                    smallint,
                game_type                   char(1),
                game_pk                     char(6),
                league                      char(2),
                location                    char(25),
                original_date               char(10),
                resume_date                 char(10),
                scheduled_innings           smallint,
                ser_games                   smallint,
                ser_home_nbr                smallint,
                series                      char(25),
                series_num                  smallint,
                stats_season                char(4),
                tbd_flag                    boolean,
                tiebreaker_sw               boolean,
                time                        char(5),
                time_date                   char(20),
                ampm                        char(2),
                time_zone                   char(3),
                venue                       char(50),
                venue_id                    char(4),
                status_ind                  char(3),
                status_reason               char(25),
                status                      char(25),
                PRIMARY KEY (gid)
            );
    ''');
    curs.execute(str_execute);

    if not silent:
      print("\tcreated games table successfully.");

    conn.commit();
    conn.close();
    if not silent:
        print("closed gameday database successfully.\n");

def createInningsTable(conn= None, verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for innings
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """
    disconnect= False;

    if conn is None:
        disconnect= True;

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS inning
            (
                gid                         char(30),
                inn_no                      smallint,
                away_runs                   smallint,
                home_runs                   smallint,
                away_score                  smallint,
                home_score                  smallint,
                PRIMARY KEY (gid, inn_no)
            );
    ''');
    curs.execute(str_execute);

    if not silent:
        print("\tcreated innings table successfully.");

    conn.commit();

    if disconnect:
        conn.close();
        if not silent:
            print("closed gameday database successfully.\n");

def createAtbatsTable(verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for atbats
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS atbat
            (
                gid                         char(30),
                inn_no                      smallint,
                inn_half                    halfinning,
                numAB                       smallint,
                batter                      char(6),
                pitcher                     char(6),
                b_height                    smallint,
                away_team_runs              smallint,
                home_team_runs              smallint,
                away_runs_pre               smallint,
                home_runs_pre               smallint,
                b                           smallint,
                s                           smallint,
                o                           smallint,
                event                       char(50),
                event_num                   smallint,
                p_throws                    side,
                stand                       side,
                start_tfs_zulu              char(24),
                end_tfs_zulu                char(24),
                play_guid                   char(50),
                des                         text,
                o_pre                       smallint,
                runs_scored                 smallint,
                rbi                         smallint,
                earned_runs                 smallint,
                on_1B_pre                   boolean,
                on_2B_pre                   boolean,
                on_3B_pre                   boolean,
                on_1B_post                  boolean,
                on_2B_post                  boolean,
                on_3B_post                  boolean,
                PRIMARY KEY (gid, inn_no, inn_half, numAB)
            );
    ''');

    curs.execute(str_execute);

    if not silent:
        print("\tcreated atbats table successfully.");

    conn.commit();
    conn.close();
    if not silent:
        print("closed gameday database successfully.\n");

def createRunnersTable(verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for runners
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS runner
            (
                runner                  char(6),
                base_start              char(5),
                base_end                char(5),
                event                   char(50),
                score                   boolean,
                rbi                     boolean,
                earned                  boolean,
                inn_no                  smallint,
                inn_half                halfinning,
                gid                     char(30),
                numAB                   smallint,
                event_num               smallint,
                PRIMARY KEY (gid, inn_no, inn_half, numAB, event_num)
            );
    ''');

    curs.execute(str_execute);

    if not silent:
        print("\tcreated runners table successfully.");

    conn.commit();
    conn.close();
    if not silent:
        print("closed gameday database successfully.\n");

def createPitchesTable(verbose= False, silent= False):
    """
    create a postgresql table in the gameday database for pitches
    if it doesn't already exist!

    parameters
    ----------
    verbose : boolean
        explicit?
    silent : boolean
        or quiet?
    """

    (conn, curs)= openGamedayDatabase(verbose= verbose, silent= silent);

    str_execute= ('''
        CREATE TABLE IF NOT EXISTS pitch
            (
                des                     text,
                pitch_id                smallint,
                type                    char(2),
                tfs                     char(6),
                tfs_zulu                char(24),
                x                       real,
                y                       real,
                start_speed             real,
                end_speed               real,
                pfx_x                   real,
                pfx_z                   real,
                px                      real,
                pz                      real,
                x0                      real,
                y0                      real,
                z0                      real,
                vx0                     real,
                vy0                     real,
                vz0                     real,
                ax                      real,
                ay                      real,
                az                      real,
                break_y                 real,
                break_angle             real,
                break_length            real,
                pitch_type              char(2),
                nasty                   real,
                spin_dir                real,
                spin_rate               real,
                inn_no                  smallint,
                inn_half                inninghalf,
                numAB                   smallint,
                on_1B                   char(6),
                on_2B                   char(6),
                on_3B                   char(6),
                count                   char(3),
                code                    char(2),
                play_guid               char(50),
                event_num               smallint,
                gid                     char(30),
                url                     text,
                PRIMARY KEY (gid, inn_no, inn_half, numAB, pitch_id)
            );
    ''');

    curs.execute(str_execute);

    if not silent:
        print("\tcreated pitches table successfully.");

    conn.commit();
    conn.close();
    if not silent:
        print("closed gameday database successfully.\n");
