# GamedayScraper

A fast, robust Python scraper for the MLB Gameday database. The idea is to be
able to do everything that Carson Sievert's
[`pitchRx`](https://pitchrx.cpsievert.me/) does, but in a native Python
implementation.

The project adds on top of the capabilities of `pitchRx` in three primary ways:
1.  automatic updating: when the database already exists, `GamedayScraper` will
    _update_ data in rows that are present. This is to allow for post-hoc
    data correctionsn in season.
1.  fault tolerance: when any data can't be downnloaded or written correctly,
    the mistake is logged. Hopefully, this will mean no missing data, at least
    none lost in translation.
1.  nominally, the Python scripts here will do the whole job faster. In fact,
    I think it's fast enough to do pull in a historical database in a work
    session and dispose of it at the end. In my experience with `pitchRx` this
    was not possible: running on my personal laptop, while streaming video, it
    filled the database in just over eight minutes.


